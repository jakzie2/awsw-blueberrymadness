# AwSW - The Blueberry Madness

The Blueberry Madness is a work-in-progress mod for the amazing game Angels with Scaly wings, which adds a new interesting experience by introducing an unusual character, changing the direction of the whole storyline, and obliterating the fourth wall into pieces.

If you enjoy silly content, stupid puns, sarcasm, 4th wall breaking, cursed edits, fun minigames, blueberries or just feel like the story is too serious, then this is the mod for you.

Because the mod is in an early stage, it's currently very short and it changes only Chapter 1 and a part of Chapter 2. However, the story can branch in several ways depending on your choices, so you'll need to play it multiple times to see everything.

More info: [http://jakzie2.8u.cz/awsw_mods/bkevin](http://jakzie2.8u.cz/awsw_mods/bkevin)
