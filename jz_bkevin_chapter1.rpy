label jz_bkevin_no_lorem:
    nvl clear
    hide meetinglorem with fade
    $ renpy.pause (1.0)
    
    stop music
    play sound "fx/system.wav"
    
    s "{i}System message{/i}: An error occured while loading a character \"Lorem\". No character image found. Proceeding to the next scene isn't possible."
    
    $ renpy.pause (1.0)
    
    s "Oh, you can see those? The system messages?"
    
    menu:
        "Yes":
            s "..."
            s "You shouldn't be able to do that... let me fix it."
        "No":
            $ jz_bkevin_system_points -= 1
            
            s "Good."
            
            $ renpy.pause(1.0)
            
            s "Wait..."
            s "You're lying, I've checked and I'm pretty sure you can see them."
            s "Please don't lie to me again.{w} Now, let me fix it."
    
    $ renpy.pause (2.0)
    
    s "Alright, you won't be able to see them anymore."
    s "Anyway, I'm experiencing some troubles loading the next scene because of a missing image. It's probably just a random glitch, I will try to load it again."
    s "I apologize for the inconvinience."
    
    $ renpy.pause (2.0)
    play music "mx/basicguitar.ogg" fadein 1.0
    
    show meetinglorem at Pan((540, 0), (540, 608), 10.0) with fade
    $ renpy.pause (8.5)
    
    hide meetinglorem with fade
    $ renpy.pause (1.0)
    
    stop music
    play sound "fx/system.wav"
    
    s "..."
    
    $ renpy.pause (1.0)
    
    s "It still doesn't work. But don't panic, I can solve this. I have plenty of experience with problems like this one."
    s "Just wait a moment please."

    play sound "fx/writing.ogg"
    $ renpy.pause (3.0)
    
    s "Done.{w} Problem solved."
    s "I would tell you how I did it, but I used several highly clever and incredibly complex alogorithms which your brain wouldn't be able to comprehend."
    s "Apologies for making you wait. Enjoy the game."
    
    $ renpy.pause (2.0)
    play music "mx/basicguitar.ogg" fadein 1.0
    
    show meetinglorem at Pan((540, 0), (540, 608), 10.0) with fade
    $ renpy.pause (8.5)
    
    jump jz_bkevin_no_lorem_return
    

label jz_bkevin_blueberry:
    c "(Do you know why blueberries don't drive?)"
    c "(Because they alw-)"
    c "Hold on, this isn't a real blueberry!"
            
    m "While holding that weird blueberry in my hands, I began to realize that it wasn't as squashy as the real ones, but hard and solid."
    m "It wasn't just unusually large, but also felt and looked like painted wood."
    m "Suddenly a strong shivering started going through my whole body. I began to feel dizzy and had to lean on a cupboard to not fall on the ground."
            
    $ renpy.pause (2.0)

    stop music
    show kitchengl
            
    $ renpy.pause (2.0)
            
    "???" "..."
    "???" "Oh... hello?"
    "???" "Who is that?"
    "???" "Hey! It's you!"
    "???" "I know why blueberries don't drive."
    "???" "It's because they always end up in a traffic jam!"
    "???" "HAHAHAHA."
    "???" "I love that joke."
    "???" "Anyway, I need somet-"
    "???" "Oh no!"
    "???" "They're onto me. I need to go now."
    "???" "Bye."
            
    scene black with dissolvemed
            
    $ renpy.pause (1.0)
            
    play sound "fx/impact.wav"
            
    $ renpy.pause (3.0)
    
    play sound "fx/system.wav"
    s "Don't worry, I have...{w=0.5} everything under control! Just another small glitch, nothing serious... I swear."
    s "Please don't touch that thing if you ever see it again."
            
    $ renpy.pause (1.0)
            
    scene kitchen with dissolvemed
    play music "mx/basicguitar.ogg" fadein 1.0
            
    $ renpy.pause (0.5)
            
    c "(Ouch, that was weird. I must've passed out.)"
    c "(I'm really lucky my head didn't hit a corner of that cupboard.)"
    c "(Wait, the blueberry is gone...{w} Was it all just a halucination? Maybe some side effect of the portal?)"
            
    $ renpy.pause (1.0)
            
    $ jz_unblueberry = False
            
    jump menukitchen


label jz_bkevin_after_inv:
    Br "There's one more thing I need to show you."
    
    m "He lifted his hand towards me and opened it, revealing a familiar object. The same wooden blueberry I encountered before was sitting in his hand."
    m "I didn't know what to think about it, maybe it wasn't a halucination after all?"
    
    Br "We found this on the floor next to the dead body."
    Br "It may not be important at all, but if it was something Reza had, it could serve as an evidence he was here during the murder."
    Br "Have you ever seen him with this... blueberry thing?"
    
    c "N-no... I don't think this is his... There's no point of him carrying such a thing."
    
    Br brow b "Are you certain? You seem a bit unsure."
    
    m "My confusion was clearly visible, but I didn't want to tell Bryce the details. He would most definitely think I'm crazy."
    
    c "Yes, I'm certain."
    
    Br "Really? You look confused."
    
    $ jz_bkevin_throw_count = 0
    
    label jz_bkevin_throw:
    
        c "Uhm, I'm still a bit confused about the whole situation with Reza. I'd never expect him to act like this."
    
        Br normal b "Understandable. Don't worry, we're gonna solve this eventually."
        Br stern b "What should I do with this piece of wood then?"
    
        menu:
            "Throw it away."if jz_bkevin_throw_count <= 3:
                $ jz_bkevin_not_throw = False
                
                Br "Are you sure? If it belongs to someone else than Reza, they may be looking for it."
                
                c "It probably belonged to the dragon who was killed. They can't really miss it anymore."
                
                Br normal b "Fair point. However, we should still put it on a visible place, so if someone else dropped it and comes here, they can easily find it."
                
                m "He walked to the edge of the path and placed the blueberry on the ground, so it was still clearly visible from a short distance."
                
                Br "Done."
            
            "Give it to me." if jz_bkevin_throw_count <= 2:
                $ jz_bkevin_not_throw = True
                
                Br "Why? What are you gonna do with it?"
                
                c "Not sure yet, but I kinda like how it looks. I can place it on a shelf or so."
                
                Br "Hmmm. If it belongs to someone else than Reza, they may be looking for it."
                
                c "It probably belonged to the dragon who was killed. They can't really miss it anymore."
                
                Br normal b "Fair point. However, if its owner comes to us, you're gonna have to give it back."
                
                c "Understood."
                
                m "He handed me the blueberry and I took it in my hands. It was definitely the same one I found in my apartment. I held it and looked at it for a bit, waiting to see if I passed out again."
                m "Nothing happened, so I put the blueberry into my pocket."
                
            "CLICK-ME" if jz_bkevin_throw_count >= 2:
                $ renpy.pause (2.0)
                
                stop music
                hide bryce
                show town6brgl
                
                $ renpy.pause (2.0)
                
                "???" "..."
                "???" "Hey, it worked!"
                "???" "I made them inactive, so I don't have to be hiding anymore."
                "???" "Well, it's only temporary, they can come back at any moment."
                
                if jz_bkevin_throw_count <= 3:
                    $ jz_bkevin_bkev_points += 1
                    
                    "???" "You did exactly what I needed you to do, without even telling you."
                    "???" "Good job."
                    
                else:
                    "???" "It took a lot of effort to make you do what I needed you to."
                    "???" "But it worked eventually."
                
                "???" "Anyway, how are you?"
                "???" "Doing good?"
                "???" "I hope you are."
                "???" "Do you have any idea what this game is about?"
                "???" "I don't."
                "???" "But it seems too serious."
                "???" "I don't like serious."
                "???" "Serious is just another term for boring."
                "???" "Hmmm..."
                "???" "Either that Bryce guy went through a really messed up plastic surgery or this game is broken."
                "???" "Most likely the second option."
                "???" "Definitely not my fault."
                "???" "But I believe I can fix it."
                "???" "I've no idea what scene to play next, so..."
                "???" "Gonna play a random one."
                "???" "What could possibly go wrong, right?"
                "???" "Nobody can stop me anyway..."
                "???" "...for now."
                "???" "Gimme a second."
                
                $ renpy.pause (1.0)
                
                "???" "Eeny, meeny, miney..."
                
                $ renpy.pause (1.0)
                
                "???" "This one it is then."
                "???" "Sadly I can't talk to you when a scene is playing..."
                "???" "...for now."
                "???" "I will find a way around."
                "???" "See ya."
                
                scene black with dissolvemed
                
                $ renpy.pause (2.0)
                
                play music "mx/elegant.ogg" fadein 1.0
                scene town3 with dissolveslow
                
                $ renpy.pause (2.0)
                
                jump jz_bkevin_after_inv_end
        
        c "Good, is that all?"
        
        stop music
        
        $ renpy.pause (3.0)
        
        if jz_bkevin_throw_count == 1:
            play sound "fx/system.wav"
            s "Apparently there's a glitch, causing this sequence to repeat itself."
            s "I'm sorry, this amount of problems doesn't normally occur, but we're experiencing some...{w=0.5} uhm...{w=0.5} technical issues today."
            s "It shouldn't damage your game experience too much."
            
            if jz_bkevin_not_throw:
                s "Also I told you not to touch that quarant- I mean blueberry thing again."
                s "I advise you to listen to me in this regard."
            
            s "Now, I'm gonna attempt to break the loop, be patient please."
            
            $ renpy.pause (3.0)
            
        elif jz_bkevin_throw_count == 2:
            $ jz_bkevin_bkev_points -= 1
            
            play sound "fx/system.wav"
            s "It seems like my attempt has been unsuccessful and it still loops back. Let me try again."
            
            $ renpy.pause (3.0)
            
        elif jz_bkevin_throw_count == 3:
            $ jz_bkevin_bkev_points -= 1
            $ jz_bkevin_system_points += 1
            
            play sound "fx/system.wav"
            s "..."
            s "Let's try it one more time, it has to work now."
            s "I'm glad for your patience."
            
            $ renpy.pause (3.0)
        
        play music "mx/basicguitar.ogg" fadein 1.0
        $ jz_bkevin_throw_count += 1
        jump jz_bkevin_throw


label jz_bkevin_kevinmenu:
    menu:
        "Invite him." if jz_bkevin_kevinmenu_first or jz_bkevin_kevinmenu_stage == 8:
            $ jz_bkevin_kevinmenu_stage = 0
            $ jz_bkevin_kevinmenu_first = True
        
        "Invite hmi." if jz_bkevin_kevinmenu_stage == 1:
            $ jz_bkevin_kevinmenu_stage += 1
            
        "Ivnite him." if jz_bkevin_kevinmenu_stage == 2:
            $ jz_bkevin_kevinmenu_stage += 1
            $ jz_bkevin_kevinmenu_first = True
            
        "Invtie him." if jz_bkevin_kevinmenu_stage == 4:
            $ jz_bkevin_kevinmenu_stage += 1
            $ jz_bkevin_kevinmenu_first = True
            
        "Inivte him." if jz_bkevin_kevinmenu_stage == 7:
            $ jz_bkevin_kevinmenu_stage += 1
        
        "Excuse yourself." if not jz_bkevin_kevinmenu_first:
            $ jz_bkevin_kevinmenu_stage = 0
            $ jz_bkevin_kevinmenu_first = True
        
        "Excuse yuosrelf." if jz_bkevin_kevinmenu_stage == 0:
            $ jz_bkevin_kevinmenu_stage += 1
            $ jz_bkevin_kevinmenu_first = False
            
        "Exucse yourself." if jz_bkevin_kevinmenu_stage == 3:
            $ jz_bkevin_kevinmenu_stage += 1
            $ jz_bkevin_kevinmenu_first = False
            
        "Ecxuse yourself." if jz_bkevin_kevinmenu_stage == 5:
            $ jz_bkevin_kevinmenu_stage += 1
            
        "Excuse yoruself." if jz_bkevin_kevinmenu_stage == 6:
            $ jz_bkevin_kevinmenu_stage += 1
            $ jz_bkevin_kevinmenu_first = False
        
        "Give him the blueberry." if jz_bkevin_kevinmenu_stage == 8:
            
            c "Here, take this."
            
            m "I reached into my pocket and pulled out the weird blueberry which Bryce found next to the victim."
            
            Kv brow "What is that? A blueberry?"
            
            c "It's not a real blueberry. I found it in my apartment recently."
            c "I'm not sure what it is, but it looks like a piece of wood painted blue to resemble a blueberry."
            
            Kv normal "Interesting. Let me look at it closely."
            
            m "He took the blueberry into his hand and held it close to his face."
            
            Kv "{cps=*.2}...{/cps}"
            
            Kv ramble "Well, I can now confirm with absolute certainty this isn't a real blueberry."
            
            c "I just told you that..."
            
            Kv normal "True, but having a direct evidence is much safer than taking someone's word for granted."
            Kv ramble "For example, I didn't believe humans are real until one of them arrived through the portal. Although I still had my doubts when I first heard about it."
            Kv "But seeing you standing in front of me is a clear evidence that humans {b}do{/b} actually exist."
            
            c "That makes sense, I guess. It was the same for me, I didn't absolutely believe this world is real until I went through the portal."
            
            Kv smile "I'm glad you agree."
            Kv brow "Why do you want me to have this blueberry anyway? It's literally just a piece of wood."
            
            c "I don't know, I just feel like you should have it. Take it as a gift."
            
            Kv normal "In that case... thank you. Receiving a gift from a real human doesn't happen every day."
            
            c "It sure doesn't. At least not here."
            c "Why don't you come over to my place later? I'd love to hear more about what college is all about in this world."

            Kv ramble "Then you found the right person for the job."

            c "I can't promise anything right now, but maybe I'll see you another time."

            Kv normal "For sure. I also have to finish this up here, but after that I'm free."

            c "Alright, I'll let you know."

            Kv ramble "I'm here all day today and tomorrow, so it shouldn't be hard to find me."

            c "Great. I have to get going now."
            c "Thinking about it, I can't remember why I need to get going or how I even ended up on this street in the first place."
            
            Kv normal "My guess is you just had a hard day. You should probably go get some rest."
            
            m "I tried to remember something about how I got here, but nothing came up. The last thing I could recall was Bryce showing me the blueberry and asking what to do with it."
            m "How did it even end up in my pocket? I had no clue."
            m "The only explanation about these memory losses was them being a side effect of the portal. Although they strangely seemed to be happening only near the wooden blueberry."
            m "Whatever the cause was, Kevin was right about something. It was a hard day and I felt tired."
            
            c "You're right. I'll go now and be sure to let you know if anything's up."

            Kv "Sure. See ya!"
            
            scene black with dissolvemed

            $ renpy.pause (0.5)

            scene hatchery at Pan ((0, 0), (0, 180), 3.0) with dissolveslow

            $ renpy.pause (1.3)

            c "(Here we are again.)"
            
            $ renpy.pause (0.5)
            
            c "(Wait, was I here before?)"
            m "It surely felt like I was here before, but I was certain this was the first time coming to the hatchery."
            m "I wasn't even sure, why I came here, I just felt like this was the place for me to be at right now."
            c "(Is my brain messing with me again?)"
            
            show adine normal b with dissolve
            
            $ renpy.pause (1.0)
            
            stop music
            play sound "fx/system.wav"
            
            s "Hey! What's going on here?"
            s "This scene isn't supposed to be playing."
            s "It's from a much later point in the game and you're still not far from the beginning."
            s "I need to fix this."
            
            scene black with dissolvemed
            
            $ renpy.pause (1.0)
            
            s "This is getting ridiculous, I need to solve this quickly."
            s "Maybe I should've told you this earlier, but there's more going on than random glitches."
            s "A malicious program managed to get into the game."
            s "But thanks to my advanced security protocols, I was able to lock it inside a quarantine sector."
            s "The quarantine sector is the blueberry your character found, hence my advice to stay away from it."
            s "Problem is that {b}somehow{/b} the program is still able to slightly affect the game."
            s "It even managed to incapacitate me for a moment and switch to a different scene..."
            
            $ renpy.pause (1.0)
            
            s "Actually... that may be to our advantage."
            s "The quarantine sector is now stuck at a much later point in the game."
            s "Meaning I don't have to deal with it until then and have time to figure out what to do."
            s "And you can finally enjoy the game properly."
            s "Unless it changed more stuff while I was... temporarly unavailable."
            
            $ renpy.pause (1.0)
            
            s "Anyway, the whole after-investigation scene is broken, therefore I cannot play the rest of it for you."
            s "Enjoy this automatically generated summary instead."
            s "Maverick appears. He knows that there murder. He angry nobody tell. Bryce explains it's because sick leave. Maverick angry nobody does job. Main character explains they do job. Maverick angry at main character. Bryce sends away. Maverick leave."
            s "Bryce says Maverick can whatever if no interfere. And that Maverick mind not change even if facts. Bryce and main character think Reza may did murder. They hope Reza found soon."
            
            $ renpy.pause (1.0)
            
            s "And here's the next scene..."
            
            $ renpy.pause (2.0)
            
            play music "mx/basicguitar.ogg" fadein 1.0
            scene o at Pan((0, 250), (0, 250), 0.1) with dissolvemed
            
            m "Bryce led me back to the apartment. I guess there wasn't really much for me to do in the meantime, as I was spared the more arduous parts of the investigation. Maybe I should have been glad about this, though now I had an afternoon to fill."
            m "Or was that really what happened? My memory seemed very confused at that moment. I was sure the events happened that way, but when I tried to remember specific details, there was nothing... no memories."
            m "I knew for sure that Bryce led me back to the apartment. But what path were we walking down? What did we talk about? I had no idea."
            m "I also had a weird dream-like memory of a dragon with black fur, but I was sure I hadn't met anyone like that."
            m "It definitely had to be some sort of side effect of the portal. I could only hope my brain isn't permanently damaged and my memory gets better soon."
            
            jump jz_bkevin_kevinmenu_end
    
    jump jz_bkevin_kevinmenu


label jz_bkevin_adine1:
    $ renpy.pause (1.0)
    
    stop music
    play sound "fx/system.wav"
    
    s "Sorry to interrupt you, but I've noticed that several choices from this scene somehow disappeared."
    s "Thankfully, Adine's reactions to them are still there, meaning I can generate new ones based on that. For all the following ones in this scene at least."
    s "Don't worry, you won't even recognize which ones are generated by me."
    
    if adinemood >= -1:
        s "If you don't mess up, you can still have Adine be impressed by you. Good luck."
    else:
        s "You won't be able have Adine be impressed by you anymore, but you still have a chance to make sure she won't hate you."
    
    s "This should take only few seconds..."
    
    $ renpy.pause (3.0)
    
    play music "mx/serene.ogg"
    
    jump jz_bkevin_adine1_return


label jz_bkevin_lorem1:
    $ renpy.pause (1.0)
    
    stop music
    play sound "fx/system.wav"
    
    s "Oh, the character image of Lorem is still missing..."
    s "But don't worry, I can use my complex algorithms to solve the issue again."
    s "..."
    
    $ renpy.pause (0.5)
    
    s "Yes, by \"complex algorithms\" I mean drawing the image myself."
    s "I'm not made to do stuff like drawing, so it may not be as good as the original one, but I'm trying my best..."
    s "It will surely look better this time."
    
    $ renpy.pause (3.0)
    
    play music "mx/fountain.ogg" fadein 2.0
    
    jump jz_bkevin_lorem1_return


label jz_bkevin_remyhints:
    nvl clear
    window show
    
    python:
        n("Hints:")
        n("{i}" + jz_bkevin_remy_books["The Third War"] + "{/i} is directly preceded by {i}" + jz_bkevin_remy_books["The Spark"] + "{/i}.")
        n("{i}" + jz_bkevin_remy_books["The First War"] + "{/i} is directly preceded by {i}" + jz_bkevin_remy_books["The Inception"] + "{/i}.")
        n("{i}" + jz_bkevin_remy_books["The Invention"] + "{/i} is not the third, seventh, or last book.")
        n("{i}" + jz_bkevin_remy_books["The Second War"] + "{/i} is the 4th book.")
        n("There is only one book before {i}" + jz_bkevin_remy_books["The First War"] + "{/i}.")
        n("{i}" + jz_bkevin_remy_books["The Enlightenment"] + "{/i} comes at some point after {i}" + jz_bkevin_remy_books["The Third War"] + "{/i}.")
    
    window hide
    return


label jz_bkevin_remy1_done:
    stop music
    
    $ renpy.pause(1.0)
    
    play sound "fx/system.wav"
    
    s "Sorry to interrupt your gameplay again, but I've noticed the order of Remy's books got somehow messed up."
    if remyanswers == 8:
        s "However, despite the shuffled order, you still managed to solve it and get all the books correctly."
        s "Amazing job!"
    elif remyanswers == 0:
        s "Therefore you haven't gotten a single book right and you should definitely try again."
        s "My player encouragement protocols advise me to congratulate you for at least trying."
        s "Nice try!"
        s "It seems like the hints got changed as well to match the new order, so they should help you with solving."
    else:
        s "However, despite the shuffled order, you still managed to get at least some books correctly."
        s "My player encouragement protocols advise me to congratulate you for almost succeeding but not quite."
        s "Nice work!"
        s "It seems like the hints got changed as well to match the new order, so they should help you with solving if you try again."
    
    $ renpy.pause (1.0)
    
    play music "mx/choices.ogg"
    
    jump jz_bkevin_remy1_done_return


label jz_bkevin_bryce1:
    stop music
    
    $ bryce1unplayed = False
    $ renpy.pause(3.0)
    
    play sound "fx/system.wav"
    
    s "It seems like something went wrong with this scene and it cannot be played."
    s "I'm sorry, but you'll have to choose a different person to meet with."
    
    $ renpy.pause(1.0)
    
    play music "mx/basicguitar.ogg" fadein 1.0
    
    jump chapter1chars


label jz_bkevin_kevinmeet_menu:
    menu:
        c "(More free time. What should I do?)"
        "Meet with Kevin":
            stop music fadeout 1.0
            jump kevin
