label jz_bkevin_awake:
    n "I awoke from dreams filled with fire, smoke and... blueberries? The sun shining through the window gave me a comforting feeling of familiarity, despite the portal in the distance reminding me of my purpose here."
    n "I looked towards the horizon, my view undisturbed by dividing walls. The light of the morning sun painted the sky with golden hues, a view that had become a rarity back home. It evoked a feeling of freedom and openness that was almost scary to me."
    n "While I got ready for the day, my thoughts strayed to the people I knew back home. I couldn't help but wonder how they were doing."
    nvl clear
    n "Then I started thinking about my dreams again. I remembered meeting Kevin here in the apartment, talking about the wooden blueberry I found yesterday and then... another Kevin appeared, but a blue one? And upside down?"
    n "Some parts of the dream felt clear, like they were almost real, while other parts were very blurry and I couldn't remember much from them."
    n "I was certain that the last time I saw the blueberry was when Bryce found it next to the dead body and I told him to leave it there."
    n "It was just a crazy dream, nothing else..."
    n "Or was it?"
    nvl clear
    n "I remained sitting in my apartment, looking at the wall for several long minutes, thinking about the previous days."
    n "My mind went through the events of my arrival, but as I got closer to yesterday evening, I could remember less and less."
    n "What I did remember was worrying about my brain being damaged, possibly as a side effect of the portal."
    n "A memory appeared in my head of Kevin claiming my memory loss was caused by the blueberry, but it couldn't be..."
    n "Or could it?"
    nvl clear
    n "Thinking about it made me feel more confused and unsure about what was real or not."
    n "There was definitely {i}something{/i} wrong with my brain. Maybe I should talk to Bryce about it, and hopefully he won't think I'm too crazy."
    
    jump jz_bkevin_awake_end

label jz_bkevin_bryce_expl:
    menu:
        "Sure.":
            $ jz_bkevin_bryce_status = "none"
        "Not yet.":
            c "Not yet, there's something I need to tell you."
            
            Br stern b "Sure, what is it?"
            
            c "Do you remember the blueberry you found next to the murder victim?"
            
            Br "Of course I do."
            Br normal b "Nobody came to us asking for it yet, so I suppose the previous owner either doesn't care or it belonged to the victim as you assumed."
            Br "Did anyone approach you about it?"
            
            c "No, but I think there's more to it than we thought."
            c "There's something abnormal about that blueberry."
            
            Br stern b "What are you talking about? It was just a round piece of wood."
            
            menu:
                "I think it's alive.":
                    Br laugh b "What?"
                    
                    c "It has its own consciousness. From what I experienced, it can teleport to different places and mess with one's brain."
                    
                    Br "Is this some kind of joke or were you testing the local booze last night?"
                    
                    if brycestatus == "good" and chap1pickb == "bryce":
                        Br "Oh wait, you were!"
                    
                    c "N-no. It's not a joke, and I'm not drunk."
                    c "Hey, I know it sounds a little crazy..."
                    
                    Br stern b "Only a little?"
                    
                    c "Can't be much crazier for you than the existence of humans."
                    
                    Br brow b "I had no other choice but to accept that humans were real when I met Reza."
                    Br laugh b "And no, a sentient blueberry is much more crazy than that."
                    
                    c "Can you at least hear me out, please?"
                    
                    Br stern b "I'm listening."
                    
                    menu:
                        "It was a blueberry before, but now it's a dragon.":
                            $ jz_bkevin_bryce_status = "laugh"
                            
                            Br laugh b "You have to be kidding right now."
                            Br stern b "Listen, we don't have time for joking around today. We should continue the investigation. Our priority is to find Reza as soon as possible."
                            
                            c "Hey, I'm not jok-{w=0.5} ugh, fine. I see I'm not going to convince you without some evidence."
                            
                            Br brow b "I'll be very suprised if you bring me some."
                            Br normal b "Now, are you ready?"
                            
                        "Haven't you noticed anything wrong with your memory?":
                            Br brow b "Not at all, my memory is completely fine."
                            Br laugh b "That may be an overstatement. It's not any worse than usual."
                            
                            c "Are you sure? No blackouts or unclear and messed up memories?"
                            
                            Br stern b "I haven't noticed anything like that."
                            
                            if brycestatus == "good":
                                show bryce laugh b with dissolve
                                
                                Br "Unless you count our evening in the bar, of course."
                            
                            Br brow b "Why should there be something wrong with my memory, anyway?"
                            
                            c "Me and Kevin both touched the blueberry and we both have issues with our memory. You touched it too, so we assumed you would have been affected as well."
                            
                            Br "I don't know who Kevin is, but I assure you, it is just an ordinary piece of wood and it definitely didn't have any negative effects on me."
                            
                            jump jz_bkevin_bryce_expl_ask
                    
                "Uhm... can we go look at it again?":
                    Br brow b "..."
                    Br "If you bring it here quickly..."
                    
                    c "I meant that we're gonna go there together."
                    
                    Br stern b "It's gonna be faster if you just bring it to me."
                    
                    c "Wait, you want me to run all the way there across the town and bring it?"
                    
                    Br brow b "Run where?"
                    
                    c "To the house where the murder happened."
                    
                    Br stern b "You're confusing me, [player_name]. As far as I remember, you liked the blueberry and took it with you, so I guess you still have it here somewhere."
                    
                    menu:
                        "That's not what happened.":
                            Br brow b "Yes it is."
                            
                            c "No it's not. I told you to leave it there so its owner can potentially return and take it back."
                            
                            Br "I'm certain you took it with you. Or did you forget about it?"
                            
                            m "I stopped for a moment, thinking about the events in question. Suddenly a memory of Bryce's version came up to my head."
                            
                            Br "Hm...?"
                            
                            c "I'm not sure, but I think I remember both versions, however, they both feel equaly real."
                            c "That's impossible though, isn't it?"
                            
                        "I... guess you're right?":
                            Br brow b "Of course I am."
                            Br "Why do you think otherwise?"
                            
                            c "I'm not sure... I had very weird dreams last night, so I must've confused one of them with reality."
                    
                    m "He kept staring at me quietly with a judging and worried look. I realized how confused he had to be, but in that moment, I was most likely still much more confused about the whole situation than him."
                    
                    label jz_bkevin_bryce_expl_ask:
                    
                    Br stern b "Listen, I know this isn't easy for you. Your friend shot a police officer, ran away and became a murder suspect."
                    Br "However, you're still our best chance to find him, therefore we need you in a good mental health."
                    Br "Answer my question and be absolutely honest with me right now. Are you going to be alright or do we have a problem here?"
                    
                    menu:
                        "Everything is alright.":
                            $ jz_bkevin_bryce_status = "ignore"
                            
                            Br normal b "If so, get yourself together quickly, because we have important work to do."
                            
                            c "I agree, we need to find Reza as fast as possible."
                            
                            Br "Exactly. Are you ready?"
                            
                        "There's something very wrong with me...":
                            $ jz_bkevin_bryce_status = "worry"
                            
                            Br brow b "Is that so?"
                            
                            c "My memory hasn't worked properly since I came to this world, I sometimes have blackouts and forget why I do things."
                            c "It started about a day after I arrived. I recall some of the event inacurately, not at all, or remember events which didn't happen and make no sense."
                            c "The most likely explanation I have is it being a negative side effect of the portal."
                            
                            Br "Interesting."
                            Br stern b "If it really is caused by the portal, then we could assume Reza is having similar symptoms, which would explain him acting so strangely."
                            
                            c "I'm not sure, he hasn't mentioned anything about having memory problems."
                            
                            Br "You haven't mentioned it either until now."
                            
                            c "You're right. Maybe the only reason he shot Maverick and ran away is because his brain got damaged and he remembered things differently from how they actually were, but hadn't realized it."
                            
                            if jz_bkevin_kevin_talk:
                                m "I felt like I had a very similar conversation with someone else already. Most likely with Kevin, but I still wasn't sure about it being real, considering the things which had happened during it."
                            
                            Br "We won't be sure until we find him. I know it would be easy to justify all of his actions with it, but it doesn't change anything for him being potentially dangerous."
                            Br "If we later prove that both off you suffered some kind of brain damage caused by the portal, things may end up much better for Reza... and you."
                            Br "For now, we need your help with the investigation. I need to know that your judgement isn't messed up, so if you get any more blackouts or any other memory problem, don't be afraid to tell me about it as soon as you realize it."
                            
                            c "Alright, I understand."
                            
                            Br normal b "Good, are you ready?"
            
    jump jz_bkevin_bryce_expl_return


label jz_bkevin_investigation:
    if chapter2sectionsplayed == 0:
        $ renpy.pause(0.5)
        
        BKv "Hey! Wait for me!"
        
        $ renpy.pause(0.5)
        
        show bkevin normal with easeinright
        
        BKv "You didn't think you were gonna go have fun without me, did you?"
        
        if jz_bkevin_bryce_status == "laugh":
            c "You! I knew you weren't just a dream!"
            
            BKv smile "Indeed I wasn't."
        elif jz_bkevin_bryce_status == "worry":
            c "You! I was sure you were just a dream!"
            
            BKv brow "Why would you assume such a thing?"
        else:
            c "You! So you weren't just a dream..."
            
            BKv normal "Of course I wasn't."
        
        BKv ramble "Although maybe I've been in someone's dream, or I will be in the future, but this time it wasn't the case."
        
        if jz_bkevin_bryce_status == "none" or jz_bkevin_bryce_status == "ignore":
            c "Wait right here, I need to show you to Bryce."
            c "He's on the way back to the police station, but we can hopefully still catch him."
            
            BKv ramble "Nah, talking to him sounds like a waste of time anyway."
        else:
            c "Wait right here, I need to show you to Bryce so he doesn't think I'm crazy anymore."
            c "He's on the way back to the police station, but we can hopefully still catch him."
            
            BKv think "Maybe he's right and you, in fact, are crazy."
            
            c "...what?"
            
            BKv ramble "Nevermind, talking to him sounds like a waste of time anyway."
        
        BKv normal "If I'm not wrong, you have an investigation to do now. So let's get it started with."
        
        c "Yeah, I have a list of places I need to visit to look for clues and question people."
        
        BKv smile "Oh, talking with people is my speciality. They'll never forget this day when Blueberry Kevin and [player_name] questioned them."
        
        c "Wait, you want to go with me?"
        
        BKv normal "Of course, it's gonna be fun!"
        
        menu:
            "It will, most likely.":
                $ jz_bkevin_bkev_points += 1
                
                c "Investigating can sure be a lot of fun, if you're the right type of person to enjoy it."
                
                BKv "I've never done any investigating, but I'm convinced that we'll enjoy it."
            "This is a police investigation, not a party.":
                c "This is a serious investigation, I'm expected to act like a professional, not joke around."
                
                BKv brow "Are you always this boring, or just today?"
        
        show bkevin normal with dissolve
        
        menu:
            BKv "Anyway, can I go with you?"
            "Yes.":
                c "Yes you can go."
            "Uncertain yes.":
                c "I'm not sure it's a good idea, but if you won't cause too much troubles, it should be fine. So I guess you can go."
            "No but sarcasm.":
                c "Naah, you {i}definitely{/i} cannot go with me because I {i}totally{/i} won't allow it. Not a {i}smallest{/i} chance."
                
                BKv think "Is that a yes?"
                
                c "I guess."
            "No but actually yes.":
                c "I don't think I want you to come with me, but I still have some questions for you, so I guess you can join me for a bit."
            
        BKv smile "Thank you! You won't regret it."
        
        c "I really hope so."
        
        menu:
            c "(What should we do?)"
            "Visit the grocery store." if False:
                BKv "Good choice, I love shopping!"
                
                play sound "fx/steps/clean2.wav"
                scene black with fade
                jump chap2store
            "Visit the production facility.":
                BKv think "Hmmm... a production facility. What does it even produce?"
                
                c "Generators."
                
                BKv think "What kind of generators?"
                
                c "Well... the ones I need to bring back to my world in order to help us survive. We're running out of power."
                
                BKv normal "Good enough for me."
                
                play sound "fx/steps/clean2.wav"
                scene black with fade
                jump jz_bkevin_chap2facility
            "Visit the library." if False:
                BKv think "Library? How are we supposed to talk to people in the place where you should be quiet?"
                
                c "We just can't talk too loudly. And we're gonna be doing an official investigation there anyway."
                
                BKv normal "Right, we're detectives now, we don't have to follow the rules."
                
                c "That's... not really how it works, but... let's just go."
                
                play sound "fx/steps/clean2.wav"
                scene black with fade
                jump chap2library
            "Visit Tatsu Park." if False:
                BKv normal "There's never a bad time for a nice walk in the park."
                
                play sound "fx/steps/clean2.wav"
                scene black with fade
                jump chap2park
        
    else:
        jump jz_bkevin_investigation_return
