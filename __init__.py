import random
from modloader import modast, modinfo
from modloader.modclass import Mod, loadable_mod
from renpy import config
import jz_magmalink as ml

@loadable_mod
class AWSWMod(Mod):
    name = "The Blueberry Madness"
    version = "0.2.0"
    author = "Jakzie"
    dependencies = ["MagmaLink", "CRAP"]
    
    def mod_load(self):
        config.main_menu_music = None
        
        # status overlay
        #ml.Overlay() \
        #    .with_align(yalign=0.655) \
        #    .add_status("image/ui/status/bkevin_neutral.png", "Status: {b}Neutral{/b}", xalign=0.2096) \
        #    .add_status("image/ui/status/system_neutral.png", "Status: {b}Neutral{/b}", xalign=0.7904) \
        #    .compile_to("status")
        
        # blueberries in main menu
        ml.Overlay() \
            .add_image("image/ui/blueberries.png", xalign=0.33, yalign=0.235, condition="persistent.jz_bkevin_seen") \
            .compile_to("main_menu")
        
        # before main menu
        ml.find_label("before_main_menu").hook_call_to("jz_bkevin_menu")
        
        # init test
        ml.find_label("nameentry").hook_to("jz_bkevin_test")
        
        # intro skip
        ml.find_label("begingame").hook_to("jz_bkevin_skipintro")
        
        # noskip1, virus scan, no lorem
        n = ml.find_label("seccont") \
            .search_python("skipbeginning = False")
        n.search_show("cgseb") \
            .hook_from_node(n) \
            .search_python("renpy.pause (0.5)") \
            .hook_to("jz_bkevin_virusscan1") \
            .search_say("That settles it, then.", 300) \
            .hook_to("jz_bkevin_virusscan2") \
            .search_say("Luckily for us, the caf") \
            .link_from("jz_bkevin_virusscan2_end") \
            .search_say("I was reading a particularly exciting scene, in which the hero", 300) \
            .search_python("renpy.pause (8.5)") \
            .hook_to("jz_bkevin_no_lorem")
        
        # blueberry in pantry
        ml.find_label("menupantry") \
            .search_menu("Lemon") \
            .edit_choice(condition="not jz_unblueberry") \
            .add_choice("Blueberry", "jz_unblueberry", "jz_bkevin_blueberry", before="[[Go back.]")
        
        # after first investigation
        ml.find_label("quest6") \
            .search_say("What do we do now?") \
            .hook_to("jz_bkevin_after_inv")
        
        # meeting kevin
        ml.find_label("c4hatchery") \
            .search_say("I turned around to see an unfamiliar face before me.") \
            .link_from("jz_bkevin_after_inv_end") \
            .search_say("Sure, but it's not just for the job, you know. That's just me.") \
            .hook_to("jz_bkevin_kevinmenu")
        
        ml.find_label("sceneselect") \
            .search_python("evilending = False") \
            .link_from("jz_bkevin_kevinmenu_end")
        
        # adine meeting 1
        ml.find_label("adine1choice1") \
            .search_menu("You're soaked, why don't you come in?").block_choice()
        ml.find_label("menadine") \
            .search_menu("Sure, come in.").block_choice() \
            .search_menu("Maybe there's more to it than that.").block_choice() \
            .search_menu("Well, {i}can{/i} you?").block_choice() \
            .search_menu("Nothing special.").block_choice() \
            .search_menu("You must be the most overqualified delivery flyer in history, then.").block_choice() \
            .search_menu("Awesome, where can I watch that?").block_choice() \
            .search_say("Okay, since it is my turn again, well...").hook_to("jz_bkevin_adine1") \
            .search_menu("Dragons are noble creatures.").edit_choice(text="Dragon - powerful and noble creature.") \
            .search_menu("I thought it was awesome.").edit_choice(text="I had very positive thoughts about it.") \
            .search_menu("It wasn't really my kind of thing.").edit_choice(text="It was bellow my subjective avarage.") \
            .search_menu("I'll check it out when I have the time.").edit_choice(text="I'll find a free time in my calendar to spend by evaluating its content.")
        ml.find_label("nextsc") \
            .search_menu("Well, you started it.").edit_choice(text="You were the initiator of this situation.") \
            .search_menu("World peace.").edit_choice(text="Peace on Earth, good will to men.") \
            .search_menu("What would you do if you were invisible for a day?").edit_choice(text="What would you do if your visibility was set to 0 for exactly 86400 seconds (approximately 0.0027 years)?") \
            .search_menu("Say my last goodbyes and hope for the best.").edit_choice(text="Say goodbye and keep my hopes at the highest possible value.")
        
        # lorem meeting 1
        ml.find_label("lorem1") \
            .search_if("brycedead == False") \
            .hook_to("jz_bkevin_lorem1")
        
        # remy meeting 1
        ml.find_label("remyhints") \
            .hook_to("jz_bkevin_remyhints", return_link=False)
        n = ml.find_label("reset")
        n.search_python("TheSecondWar = True") \
            .hook_from_node(n)
        n.hook_call_to("jz_bkevin_remyhints")
        
        remy_books = ["The Third War", "The Inception", "The Second War", "The Spark", "The Enlightenment", "The Invention", "The First War", "The Tribe"]
        remy_books_new = remy_books[:]
        random.shuffle(remy_books_new)
        remy_books_dict = dict(zip(remy_books, remy_books_new))
        modast.set_renpy_global("jz_bkevin_remy_books", remy_books_dict)
        
        remy_books_menus = [ml.find_label("remyques" + str(i+1) + "x").search_menu() for i in range(4)]
        remy_books_menus.extend([ml.find_label("remyques" + str(i+1)).search_menu() for i in range(7)])
        remy_books_menus.append(remy_books_menus[-1].search_menu())
        
        for m in remy_books_menus:
            m.node.items[:] = [(remy_books_dict[item[0]] if item[0] in remy_books_dict else item[0], item[1], item[2]) for item in m.node.items]
            random.shuffle(m.node.items)
        
        remy_books_menus[-1].search_say("Oh, you're done? Let's take a look, then.") \
            .hook_to("jz_bkevin_remy1_done")
        
        # bryce meeting 1
        c = ml.find_label("chapter1chars") \
            .search_if("chapter1csplayed == 0")
        c.branch() \
            .search_menu().branch("Meet with Bryce.") \
            .hook_to("jz_bkevin_bryce1", return_link=False)
        c.branch("chapter1csplayed == 1") \
            .search_menu().branch("Meet with Bryce.") \
            .hook_to("jz_bkevin_bryce1", return_link=False)
        
        # second meeting menu in ch1 change
        ml.find_label("chapter1chars") \
            .search_if("chapter1csplayed == 1").branch() \
            .search_scene("o") \
            .hook_to("jz_bkevin_kevinmeet_menu")
        
        m = ml.find_label("jz_bkevin_kevinmeet_menu").search_menu()
        m.node.items[:] = [m.node.items[0]] * 18
        
        # kevin meeting choice
        ml.find_label("kevin") \
            .search_say("I'll be right behind you.", 300) \
            .search_python("renpy.pause (0.5)") \
            .hook_to("jz_bkevin_xkevin_choice") \
            .search_say("Not more than anyone else, I think.") \
            .hook_to("jz_bkevin_xkevin")
        
        # before chapter2, mc awake, explaining to Bryce
        ml.find_label("chapter2") \
            .hook_to("jz_bkevin_xkevin_alt") \
            .search_say("Another day, another morning.") \
            .hook_to("jz_bkevin_awake") \
            .search_say("But I had greater things to worry about at the moment, like Reza") \
            .link_from("jz_bkevin_awake_end") \
            .search_say("Are you ready to go?") \
            .hook_to("jz_bkevin_bryce_expl")
        
        # second investigation
        ml.find_label("chapter2sections").hook_to("jz_bkevin_investigation")
        
        # after second investigation
        ml.find_label("chap2cont2") \
            .search_show("sebastian normal b") \
            .link_from("jz_bkevin_chap2seb")
        ml.find_label("chap2skip3") \
            .search_say("Anyway, you said you had some information for us") \
            .hook_to("jz_bkevin_chap2facility_seb1") \
            .search_say("You're welcome.") \
            .link_from("jz_bkevin_chap2facility_seb_end")
        
        ml.find_label("chap2skip2").hook_to("jz_bkevin_ending")
    
    def mod_complete(self):
        pass
