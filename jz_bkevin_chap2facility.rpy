label jz_bkevin_chap2facility:
    $ chapter2sectionsplayed += 1
    $ chapter2facilityunplayed = False
    scene facin2 at Pan ((0, 250), (128, 250), 3.0) with dissolveslow
    
    show bkevin normal with dissolve
    
    c "Reza was allegedly here at some point. Maybe we should ask Anna."
    
    play sound "fx/knocking1.ogg"

    $ renpy.pause (2.0)

    c "Hello?"

    play sound "fx/knocking2.ogg"

    $ renpy.pause (2.5)

    c "Anybody here?"
    
    BKv think "Looks like they don't hear you. Or don't wanna hear you."
    
    c "I don't think so, it's more likely that she's not in there right now."
    
    BKv ramble "Or you don't know how to knock properly."
    BKv smile "Let me show you my exceptional door knocking abilities."
    
    c "Hey, my knocking abilites are fine. I'm not trying to break the door, just make Anna hear us and open it."
    
    BKv ramble "Well, I must say your efforts seem to be a bit ineffective."
    BKv normal "Watch and learn."
    
    show bkevin normal flip
    
    $ renpy.pause(0.5)
    
    show bkevin normal flip at right with ease
    
    $ renpy.pause(0.5)
    
    play sound "fx/knocking2.ogg"
    queue sound "fx/knockheavy.ogg"
    
    $ renpy.pause (2.0)
    
    c "If Anna was there, she'd have opened the door by now..."
    
    show bkevin normal flip at left with ease
    show bkevin normal flip at Position(xpos=0.8) with move
    
    play sound "fx/tableslap.wav"
    
    $ renpy.pause (1.0)
    
    c "Are you sure this is a good idea?"
    
    show bkevin normal flip at left with ease
    show bkevin normal flip at Position(xpos=0.8) with move
    
    play sound "fx/craptableslaploud.wav"
    
    $ renpy.pause (0.8)
    
    show bkevin normal flip at left with ease
    show bkevin normal flip at Position(xpos=0.8) with move
    
    play sound "fx/craptableslaploud.wav"
    
    $ renpy.pause (0.5)
    
    show bkevin normal flip at left with ease
    hide bkevin with moveoutright
    
    play sound "fx/crapsmashimpact.wav"
    stop music
    
    $ renpy.pause (2.0)
    
    c "..."
    
    scene testingroom
    show damion face at right
    show bkevin normal flip at Position(xpos=-0.1, ypos=0.4):
        rotate 90
    with fade
    
    play music "mx/elegant.ogg" fadein 1.0
    $ renpy.pause(1.0)
    
    $ metdamion2 = True
    $ persistent.metdamion = True
    
    "???" "C-can I help you?"
    
    c "Ehm, we're just looking for Anna. This is her lab, right?"
    
    show damion arrogant with dissolve
    
    "???" "I've seen a lot during my work here, but I'd never expect two freaks to casually break through the door and ask for Anna without even introducing themselves."
    "???" "Do you even know how much such door costs?"
    "???" "Doesn't matter, because you're going to gain that knowledge when you pay for a new one."
    
    menu:
        "We're sorry...":
            "???" "Your apology doesn't change anything about the fact that the door is now broken."
            
            c "We're going to pay for a new one, is that right?"
            
        "You should've responded to our knocking.":
            $ jz_bkevin_bkev_points += 1
            
            "???" "I was busy. I'm doing an important job here and don't have time to talk to everyone who comes by."
            "???" "Anyway, a {i}normal{/i} person would wait a bit longer or, even better, go away. How did you even come up with the idea of breaking in here?"
            
            c "I don't know, it wasn't my idea."
    
    m "I looked down at Blueberry Kevin, who had fallen on the ground after smashing through the door."
            
    $ renpy.pause(0.5)
            
    BKv "Ehrrk..."
    
    c "Can you get up?"
    
    BKv "Why?"
    
    c "Perhaps because you're awkwardly lying on the ground?"
    
    BKv "Is that a problem?"
    
    "???" "Unless you're planning to become my cleaning rag, I'd suggest you to do it."
    
    BKv "Talking about cleaning rags, you should really get one. This floor is even more messy than the one in [player_name]'s apartement."
    
    show damion face with dissolve
    
    "???" "Who are you? A floor inspector?"
    
    BKv "The one and only. I'm the main inspector of ALL the floors, especially the ones I happen to fall on. And your floor is way below avarage, you should really feel bad about it."
    
    "???" "..."
    
    c "Just get up, please..."
    
    BKv "Okay."
    
    hide bkevin with dissolve
    show bkevin normal flip at left with dissolve
    
    c "Thank you."
    
    show damion arrogant with dissolve
    
    "???" "Now, tell me what you want here and why you're asking for Anna, or I'm going to kick you both out right now."
    
    c "We're here on an investigation regarding Reza, so we're expecting your cooperation."
    c "I expected to meet Anna here, but maybe you can help us instead."
    
    "???" "Oh, of course. Your precious Anna isn't here, so you'll have to settle for me. I see how it is."
    
    c "That's not what I meant."

    show damion normal with dissolve

    "???" "Of course, of course. Let's just get this over with."
    
    c "You could start by telling me who you are."

    "???" "Here's the short version: My name is Damion. I work in this facility, and I'm unfortunate enough to live the nightmare of having to deal with Anna on a daily basis. Nice to meet you."
    
    c "Nice to meet you, too."

    Dm "And you are?"
    
    show bkevin normal at left

    c "You don't know who I am?"
    
    hide bkevin with moveoutleft

    Dm arrogant "Tchk, of course I know who you are, but your assumption validated the point I was going to make. You see, this whole thing about you coming here has been blown out of propor-{w=0.5}{nw}"
    
    stop music
    
    Dm "Hey! What are you doing over there?"
    
    m "I turned my head and saw Blueberry Kevin holding a small microscope."
    
    BKv "Me? I'm just looking. All this stuff is very interesting."
    
    m "He threw the microscope into the air and caught it with his other hand."
    
    show damion normal with dissolve
    
    Dm "Stop it! That's an expensive piece of equipment."
    
    BKv "Oh, is it? Would be sad if someone accidentally dropped it, right?"
    
    Dm "Put it back immediately!"
    
    BKv "You'll have to catch me first, but I'm sure a fish like you won't be able to."
    
    play music "mx/yaketysax.mp3" fadein 1.0
    
    Dm "Come here, you-{w=0.5}{nw}"
    
    hide damion normal with moveoutleft
    
    BKv "Catch me if you can!"
    
    $ renpy.pause(0.5)
    
    show bkevin normal flip at Position(xpos=1.2) with moveinleft
    show damion normal flip at Position(xpos=1.2) with moveinleft
    
    Dm "Stop running!"
    
    BKv "Not a chance!"
    
    show bkevin normal at Position(xpos=-0.2) with move
    show damion normal at Position(xpos=-0.2) with move
    
    BKv "Ha! I knew you were just a slow, sloppy fish."
    
    Dm "Call me like that one more time and I'll show you-"
    
    show bkevin normal flip at Position(xpos=1.2) with move
    show damion normal flip at Position(xpos=1.2) with move
    
    c "(I have no words.)"
    
    BKv "Sloppy fish, sloppy fish, very ugly grumpy fish."
    
    show bkevin normal at Position(xpos=-0.2) with move
    show damion normal at Position(xpos=-0.2) with move
    
    c "(So much for our investigation...)"
    
    m "Blueberry Kevin ran through the broken door into the coridor with Damion following right behind him."
    
    scene facin2 with dissolveslow
    
    $ renpy.pause(0.5)
    
    show bkevin normal flip with moveinleft
    
    $ renpy.pause(0.2)
    
    show bkevin normal
    
    BKv "I guess this microscope is mine now."
    
    Dm "It definitely isn't!"
    
    show bkevin normal flip
    
    $ renpy.pause(0.2)
    
    show bkevin normal flip at Position(xpos=1.2) with move
    show damion normal flip at Position(xpos=1.2) with moveinleft
    
    BKv "What do you even need it for?"
    
    Dm "What do you think I'd use a miscroscope for? To better see very tiny objects of course."
    
    show bkevin normal at Position(xpos=-0.2) with move
    show damion normal at Position(xpos=-0.2) with move
    hide bkevin
    hide damion
    
    BKv "I bet you do. Especially in your bed."
    
    Dm "Get back here you little bastard!"
    
    show bkevin normal upflip at Position(xpos=1.2, ypos=0.7) with moveinleft
    show damion normal upflip at Position(xpos=1.2, ypos=0.7) with moveinleft
    
    c "(Did they just run on the ceiling?)"
    
    Dm "When I catch you I'm gonna turn you into a cleaning rag for the whole facility."
    
    BKv "It will never come to that, because you'll never catch me."
    
    show bkevin normal up at Position(xpos=-0.2, ypos=0.7) with move
    show damion normal up at Position(xpos=-0.2, ypos=0.7) with move
    hide bkevin 
    hide damion
    
    Dm "You can't run forever."
    
    BKv "Watch me."
    
    show bkevin normal flip at Position(xpos=1.2) with moveinleft
    show damion normal flip at Position(xpos=1.2) with moveinleft
    
    m "They ran towards the front door and straight out of the building."
    
    scene fac1 with dissolveslow
    
    $ renpy.pause(0.5)
    
    show damion normal with moveinright
    
    Dm "Don't just stand there, help me catch him!"
    
    menu:
        "I can try.":
            $ jz_bkevin_bkev_points -= 1
            $ jz_bkevin_chase_join = True
            c "I can certainly try, but no promises."
            
            Dm "Good, at least one of you is not an asshole."
        "No.":
            $ jz_bkevin_bkev_points += 1
            $ jz_bkevin_chase_join = False
            c "No, I'm gonna stay and watch you, it's better than a television."
            
            Dm "..."
    
    BKv "I'm waiting you lazy slug. You definitely won't catch me by standing there like a concrete pole."
    
    Dm "Ghrrr..."
    
    hide damion with moveoutleft
    
    BKv "How expensive is the microscope?"
    
    Dm "More than I'll have to pay for your treatment after I kick your ass for this."
    
    show bkevin normal flip at Position(xpos=1.2) with moveinleft
    show damion normal flip at Position(xpos=1.2) with moveinleft
    
    BKv "Your kicks won't hurt me much through all of my fluffy fur."
    
    Dm "Don't mind if I try anyway."
    
    show bkevin normal at Position(xpos=-0.2) with move
    show damion normal at Position(xpos=-0.2) with move
    
    if jz_bkevin_chase_join:
        Dm "Catch him, now!"
        
        m "When I saw Blueberry Kevin running towards me again, I jumped right in front of him at the very last moment."
        m "I was prepared for an impact, but it never came. He went right through me as if I wasn't there at all."
        m "I turned myself around and saw him running away from me."
        
        c "How did you...?"
        
        play sound "fx/impact3.ogg"
        stop music
        
        scene black with hpunch
        
        $ renpy.pause(2.0)
        
        m "Something hit me from behind and I fell on the ground, slamming my face into the dirt. Then I felt something fall on top of me."
        
        $ renpy.pause(0.5)
        
        Dm "Ouch."
        
        c "Hey, get off me."
        
        Dm "I'm trying to."
        
        m "He got up and I turned around. I sat down on the ground for a moment and then got up as well."
        
        scene fac1
        show damion arrogant
        with fade
        
        c "What just happened?"
        
        Dm face "I'm not sure, but it seemed like he went through you like a ghost."
        
        c "He may actually be one... But I'm not sure."
        
        BKv "Hahahaha! That was funny, can you do it again?"
        
        play music "mx/yaketysax.mp3" fadein 1.0
        
        Dm normal "Ghost or not, I'm going to get him!"
        
        show bkevin normal at Position(xpos=1.2)
        show damion normal flip
        
        $ renpy.pause(0.2)
        
    else:
        m "I stood still, letting my gaze follow the two dragons running across the landscape."
        m "Even though Blueberry Kevin's actions were inherently dumb, there was something oddly enjoyable about watching the chase."
        m "If only I had some popcorn..."
        
        show bkevin normal flip at Position(xpos=1.2) with move
    
    show damion normal flip at Position(xpos=1.2) with move
    
    Dm "When I catch you I'm going to shave all of your fur."
    
    BKv "Even if you did, I'd still look better than you."
    
    show bkevin normal flip at Position(xpos=-0.2) with move
    show damion normal flip at Position(xpos=-0.2) with move
    hide bkevin
    hide damion
    
    c "(Are they really running backwards?)"
    
    show bkevin normal up at Position(xpos=1.2, ypos=0.7) with moveinleft
    show damion normal up at Position(xpos=1.2, ypos=0.7) with moveinleft
    hide bkevin
    hide damion
    
    c "(What the...)"
    c "(This is ridiculous, we're outside, there isn't even any ceiling, they're just running on air.)"
    
    show bkevin normal at Position(xpos=-0.2) with moveinright
    show damion normal at Position(xpos=-0.2) with moveinright
    
    m "They didn't turn back this time and instead kept running down the street further away from me."
    
    c "Hey! Wait for me!"
    
    scene gate with wiperight
    
    show bkevin normal flip with moveinleft
    
    BKv "Still following us, I see."
    
    c "Of course I am."
    c "Why are you doing this? We were supposed to go there investigating, not steal people's property."
    
    BKv smile flip "I know, but this is much more fun, isn't it?"
    
    menu:
        "Not at all, please stop.":
            BKv brow flip "You just have to make everything so boring..."
            BKv ramble flip "Request denied."
            
            c "Are you serious?"
            
            BKv normal flip "I'm never serious."
            
        "Watching you two chasing each other is hilarious.":
            $ jz_bkevin_bkev_points += 1
            
            BKv "I can imagine it must be."
            BKv normal flip "Have fun, it's far from over."
    
    Dm "Ha! I got you now!"
    
    show bkevin normal flip at Position(xpos=1.2) with move
    show damion normal flip at Position(xpos=1.2) with moveinleft
    
    BKv "Only in your dreams."
    
    Dm "My dreams thankfully don't include you at all."
    
    show bkevin normal at Position(xpos=-0.2) with move
    show damion normal at Position(xpos=-0.2) with move
    
    m "They kept running away towards Tatsu Park. I had a lot of troubles keeping with them."
    c "(How do they have so much energy?)"
    
    scene town2 with wiperight
    
    show bkevin normal flip at Position(xpos=1.2) with moveinleft
    show damion normal flip at Position(xpos=1.2) with moveinleft
    
    $ renpy.pause(0.5)
    
    show bkevin normal flip at Position(xpos=-0.2) with move
    show damion normal flip at Position(xpos=-0.2) with move
    hide bkevin
    hide damion
    
    $ renpy.pause(0.5)
    
    show bkevin normal up at Position(xpos=1.2, ypos=0.7) with moveinleft
    show damion normal up at Position(xpos=1.2, ypos=0.7) with moveinleft
    
    $ renpy.pause(0.5)
    
    show bkevin normal up at Position(xpos=-0.2, ypos=0.7) with move
    show damion normal up at Position(xpos=-0.2, ypos=0.7) with move
    hide bkevin
    hide damion
    
    $ renpy.pause(0.5)
    
    show damion normal flip with moveinleft
    
    Dm "I need a rest..."
    
    show bkevin normal flip with moveinleft
    show damion normal error flip with None
    show bkevin normal coat flip at Position(xpos=1.2) with move
    
    Dm "Hey! That's my coat, give it back!"
    
    show damion normal error flip at left
    show bkevin normal coat at right
    with move
    
    BKv ramble coat "Not anymore."
    BKv smile coat "It's a nice coat, I think it fits me very well."
    BKv normal coat "I'm sure I look better in it than you."
    
    Dm arrogant error flip "No you don't. Give me my coat and microscope back immediately or I'll call the police."
    
    BKv "I see you're giving up and admitting you're a slow, sloppy fish."
    
    Dm "Definitely not, but I'm not interested in your childish shit anymore."
    
    BKv "You know what? I don't wanna listen to you anymore either. Your voice makes you sound like a drunk goose after brain surgery."
    
    Dm normal error flip "What did you just say?"
    
    BKv "You heard me."
    
    Dm "Come here right now!"
    
    BKv smile coat "Hehe, here we go."
    
    hide bkevin with moveoutleft
    show damion normal error
    
    $ renpy.pause(0.2)
    
    hide damion with moveoutleft
    
    c "(This will never end...)"
        
    show bkevin normal coat upflip at Position(xpos=1.2, ypos=0.7) with moveinleft
    show damion normal error upflip at Position(xpos=1.2, ypos=0.7) with moveinleft
    hide bkevin
    hide damion
    
    show bkevin normal coat flip at Position(xpos=-0.2) with moveinright
    show damion normal error flip at Position(xpos=-0.2) with moveinright
    
    show damion normal error flip at Position(xpos=1.2) with move
    show bkevin normal coat flip at Position(xpos=1.2) with move
    
    show bkevin normal coat at Position(xpos=-0.2) with move
    show damion normal error at Position(xpos=-0.2) with move
    show dramavian normal at Position(xpos=-0.3, ypos=0.5) with moveinright:
        zoom 0.7
    
    c "(Wait, who was that?)"
    
    show bkevin normal coat flip at Position(xpos=1.2) with move
    show dramavian normal flip at Position(xpos=1.3, ypos=0.5) with move
    show damion normal error flip at Position(xpos=1.2) with move
    
    $ renpy.pause(0.3)
    
    show dramavian normal at Position(xpos=-0.3, ypos=0.5) with move
    show bkevin normal coat at Position(xpos=-0.2) with move
    show damion normal error at Position(xpos=-0.2) with move
    
    stop music fadeout 1.0
    $ renpy.pause(0.5)
    
    play sound "fx/crapsmashimpact.wav"
    
    $ renpy.pause(2.0)
    
    m "Suddenly the small orange dragon stopped running and Blueberry Kevin fell over it, flying through the air and hitting the ground."
    m "The microscope shattered into several pieces and Damion stood above him with an upset expression."
    
    scene park2
    show damion arrogant error at right
    show dramavian normal at Position(xpos=0.8, ypos=0.5):
        zoom 0.7
    show bkevin normal coat flip at Position(xpos=-0.1, ypos=0.4):
        rotate 90
    with wiperight
    
    c "Is everyone alright?"
    
    Dr "..."
    
    BKv "Ehrrk..."
    
    play music "mx/damion.ogg" fadein 1.0
    
    Dm "No, nothing is alright. The microscope is in pieces and I will have to spend my precious time and money buying a new one."
    Dm "Also, I'm sure the police will want to have a small chat with your friend here. Let me start counting the crimes he commited today, there's break-in, vandalism, theft, harassment... and maybe some more I missed. Not looking too good for him."
    
    c "Are you being serious right now?"
    
    Dm "Of course I am."
    
    menu:
        "Oh come on, we just had a bit of fun." if jz_bkevin_bkev_points >= 5:
            $ jz_bkevin_bkev_points += 1
            
            Dm "Yes, {b}you two{/b} had fun. For me there wasn't a smallest bit of fun in having to chase him over half of the town and getting my microscope shattered."
            
            c "I'm sure you spend most of your time stuck in your lab anyway, so a little bit of running out in the fresh air was more than helpful."
            
            BKv "Exactly, you're just a sloppy fishhead sitting on your fat ass the whole day."
            
            Dm "I appreciate your worries about my health, but I do a lot of swimming in my, I admit, not so common free time."
            Dm "Anyway, I don't see how your reasoning justifies the broken microscope. Therefore, your argument is flawed and you should feel terrible."
            
            Dr "..."
            
            c "Apparently you're not in good shape when you couldn't even manage to catch him."
            
            Dm "I do {i}swimming{/i}, not {i}running{/i}. In the sea I would catch him in no time."
            
            BKv "Oh really? I take that as a challenge."
            
            $ renpy.pause(0.5)
            
            hide bkevin
            show bkevin normal coat flip at left
            with None
            
            $ renpy.pause(0.5)
            
            BKv ramble coat flip "If you manage to catch me in the sea, I'll fix your microscope and the door of your lab."
            
            Dm face error "You probably don't realize it, but the microscope is completely destroyed, it can't be fixed."
            
            Dr "..."
            
            BKv smile coat flip "Everything can be fixed if you use flex tape."
            BKv normal coat flip "And if you're not able to fix something using flex tape, you're just not using enough flex tape."
            BKv ramble coat flip "But I assume they didn't teach you this in fish school."
            
            Dm normal error "If I get my hands on a \"flex tape\", your mouth will be the first place for it to stick to. And I'll make sure I use enough of it."
            
            BKv normal coat flip "Stop making empty threats and look at the ground."
            
            Dm face error "Why?"
            
            BKv "Just do it."
            
            m "Damion and I looked on the ground where the microscope had shattered before."
            m "The pieces weren't there anymore. Instead, the small microscope laid on the ground in one piece again, looking exactly the same as when he took it from Damion's lab."
            
            c "It's... fixed?"
            
            Dr "..."
            
            Dm arrogant error "Obviously. But how is it possible? We all saw it in pieces."
            
            hide bkevin with dissolve
            
            m "Blueberry Kevin didn't answer, but leaned down to lift the microscope from the ground."
            
            stop music fadeout 1.0
            
            Dm normal error "Hey!"
            
            hide damion with dissolve
            
            m "Damion quickly stepped towards him and grabbed him by the coat, trying to get his hands on the microscope."
            m "Suddenly the blue dragon slid himself out of the coat, quickly grabbed the microscope and stepped away from Damion."
            
            show bkevin normal at left with dissolve
            hide bkevin with easeoutleft
            
            m "He started running away with the microscope again. Damion stayed in place and put on his coat."
            
            show damion normal at right with dissolve
            
            play music "mx/yaketysax.mp3" fadein 1.0
            
            Dm "Are you kidding me? Is he seriously running away again?"
            
            hide damion with easeoutleft
            
            Dr "..."
            
            scene town2 with wipeleft
            
            show bkevin normal at Position(xpos=-0.5, ypos=1.0) with moveinright
            show damion normal at right with moveinright
            
            Dm arrogant "If you think I'm going to run after you through half of the town again, you're very wrong."
            Dm "I'm getting tired of this."
            
            show bkevin flip at left with move
            
            BKv brow flip "I was heading towards the beach, but you seem too grumpy to actually follow me all the way there."
            
            stop music fadeout 1.0
            
            BKv normal flip "We're gonna have to to this the boring way then."
            
            $ renpy.pause(0.5)
            
            scene beach at Pan((0, 0), (300, 0), 5.0)
            show bkevin normal flip at left
            show damion normal at right
            with squares
            
            $ renpy.pause(0.5)
            
            BKv smile flip "Here we go!"
            
            m "At one moment we were standing in the Tatsu park but at the very next one we could feel the fresh breeze coming from the shore."
            
            play music "mx/zhongpiano.ogg" fadein 2.0
            
            Dm face "Did we just... teleport here?"
            
            BKv normal flip "Sure, if that's what you wanna call it."
            
            m "I kept looking around, still processing how we had managed to end up on the beach."
            
            c "H-how?"
            
            Dm "That's a very good question, which I'd like to know answer to as well."
            
            BKv face flip "Oh, come on, don't waste your time with pointless questions."
            BKv ramble flip "It's not important {i}how{/i} we are here, but rather {i}why{/i} we are here."
            
            Dm arrogant "Fine, why are we here then?"
            
            BKv smile flip "To begin with our race, of course!"
            
            Dm face "Our race? What are you even talking about?"
            
            BKv "Our bet! If you beat me in a swimming race I'll repeair your microscope and door."
            BKv normal flip "Did you forget or what?"
            
            Dm arrogant "I don't remember ever agreeing to that."
            
            c "Also you've already repaired the microscope anyway, meaning that losing wouldn't be as bad for him."
            
            BKv think flip "Hmmm... you're right."
            BKv smile flip "Let's fix that!"
            
            play sound "fx/glassimpact.ogg"
            
            $ renpy.pause(1.5)
            
            m "He smashed the microscope onto the ground and it shatterd into many small pieces again."
            
            c "I... guess that solves the problem?"
            
            BKv "Exactly!{w} Now, lets go!{w} Into the sea!{w}"
            
            show bkevin smile with None
            hide bkevin with moveoutleft
            
            Dm "Do you really believe I'm going to follow you in there?"
            
            BKv "If you won't join the race, it means you automatically lose."
            BKv "But I guess you're nothing but an ugly loser anyway."
            
            Dm normal "I'll show you..."
            
            hide damion with moveoutleft
            
            m "Blueberry Kevin ran towards the sea and jumped into the waves."
            m "Damion took off his lab coat, revealing the two large fins between his arms and torso."
            
            c "Now I see why he's good at swimming..."
            
            m "He jumped into the sea shortly after and I was slowly approaching the shore, trying to spot them under the water."
            
            scene underwatersea with fade
            
            python:
                def jz_bkevin_swimgame(game):
                    global jz_bkevin_swimgame_song
                    def show_obj(obj):
                        renpy.show("jz_bkevin_img" + str(obj.id), at_list=[Position(xpos=obj.xpos, ypos=obj.ypos, xanchor=0, yanchor=0)], what=obj.image)
                    
                    BKv("Hello my dear player! It's been some time since I talked directly to you.")
                    BKv("This scene would most likely be reaaaly boring for you, so I'm gonna make it more fun!")
                    BKv("Curious how? Well, you're gonna play a {b}minigame{/b}!")
                    BKv("Isn't that exciting?")
                    BKv("Okay, first we need some sprites of me and the grumpy fishhead.")
                    BKv("Hmmm... \"fishhead\"... that gave me an idea... let's use our heads!")
                    
                    renpy.pause(0.5)
                    
                    show_obj(game.bkevin)
                    show_obj(game.damion)
                    
                    renpy.pause(0.5)
                    
                    BKv("Perfect!")
                    BKv("We'll be swimming from the left side to the right side and whoever gets there first, wins.")
                    BKv("Now, because your character doesn't look like he'd wanna swim with us, I have to make you another character.")
                    BKv("You'll be...{w=0.2} a fish! Because fish are good at swimming, right?")
                    BKv("Well, except Damion of course.")
                    BKv("Anyway, here's your character.")
                    
                    renpy.pause(0.5)
                    
                    show_obj(game.fish)
                    
                    renpy.pause(0.5)
                    
                    BKv("Very handsome fish, indeed. Just look at that adorable smile!")
                    BKv("After the game starts, you can use your {i}left{/i} and {i}right{/i} arrow keys to rotate yourself around and the {i}up{/i} arrow key to go forwards.")
                    BKv("You can't swim backwards, because only losers go backwards! And you aren't a loser.{w} Usually.")
                    BKv("Hmm... the water looks boring, let's add something there to make it more colorful.")
                    BKv("Obstacles! Yay!")
                    
                    renpy.pause(0.5)
                    
                    for vine in game.bad_vines:
                        show_obj(vine)
                    
                    renpy.pause(0.5)
                    
                    BKv("This is some... well.. weird red spiky junk, which is very bad!")
                    BKv("It's very bad because it slows us and you down, so try to avoid it!")
                    BKv("Now that we have bad.. stuff, let's add some good stuff too.")
                    
                    renpy.pause(0.5)
                    
                    for vine in game.good_vines:
                        show_obj(vine)
                        
                    renpy.pause(0.5)
                    
                    BKv("This is... weird green non-spiky junk! It's very good, because it makes us and you go faster!{w} Nyooom!")
                    BKv("So we have bad stuff and good stuff. Perfectly balanced as all things should be.")
                    BKv("Now I realized...")
                    BKv("You would be able to swim around, but not really be able to do anything else...")
                    BKv("I know! Let's have something randomly appear, something you'll be collecting.")
                    BKv("Apples, because everyone likes apples.")
                    BKv("Green fresh tasty apples, which make you go faster for a while when you collect them.")
                    BKv("And red ugly half-rotten apples from the bottom of your bag, which make you go slower instead.")
                    BKv("Wait, that has nothing to do with our race tho...")
                    BKv("I can make the apples work on us too then.")
                    BKv("But it still needs more... lemme think.")
                    
                    renpy.pause(1.0)
                    
                    BKv("Yes! If you press spacebar while under the effect of an apple, you can shoot that apple at us.")
                    BKv("Which allows you to mess with our race and affect its outcome.")
                    BKv("Don't ask me how is it possible to both eat and shoot the same apple... It's just a game.")
                    BKv("It doesn't have to make sense, if it just works.")
                    BKv("Hm, this piano music is kinda boring. Do you want me to keep it or change it to something else?")
                    
                    if renpy.display_menu([("Keep this music.", "keep"), ("Play something else.", "change")]) == "keep":
                        jz_bkevin_swimgame_song = False
                        BKv("Alright, if you fish...")
                    else:
                        jz_bkevin_swimgame_song = True
                        BKv("Good choice! I found some track called \"funness\". I'm sure you're gonna love it!")
                        BKv("I'll play it when the game start.")
                    
                    BKv("So, that's all, if you haven't understood something, figure it out yourself.")
                    BKv("Have fun playing! Remember, who'll be the winner is in your hands.")
                    BKv("3...{w} 2...{w} 1...{w} Go!{nw}")
                    
                    renpy.scene()
                    renpy.show("underwatersea")
                    ui.add(game)
                
                jz_bkevin_swimgame(JZBKevinSwimGame())
            
            if jz_bkevin_swimgame_song:
                stop music
                play music "mx/funness.ogg"
            
            $ jz_bkevin_swimgame_result = ui.interact(suppress_overlay=True, suppress_underlay=True)
            
            if jz_bkevin_swimgame_song:
                stop music fadeout 1.0
            
            scene beach at Pan((0, 0), (300, 0), 5.0)
            show bkevin normal flip at left
            show damion normal at right
            with fade
            
            if jz_bkevin_swimgame_song:
                play music "mx/zhongpiano.ogg" fadein 1.0
            
            $ renpy.pause(0.5)
            
            if jz_bkevin_swimgame_result == "bkevin":
                $ jz_bkevin_bkev_points += 1
                
                BKv smile flip "I was right, you're slow even in the water."
                
                Dm face "I'm sure you cheated... somehow."
                
                BKv ramble flip "Oh, come on, do I really look like the kind of guy who'd cheat in a race?"
                
                Dm face "Ehm..."
                
                c "Well..."
                
                BKv face flip "Forget I asked."
                BKv smile flip "Anyway, you lost the race, do you know what that means?"
                
                Dm arrogant "You're finally going to stop bothering me?"
                
                BKv think flip "Hm, maybe."
                BKv ramble flip "But most importantly, you lost our bet, therefore both the microscope and the door stay broken."
                
                Dm "Whatever, but don't be suprised when the police knocks on your door at some point."
                
                BKv smile flip "Looking forward to meet them! I've always wanted to play some board games with police guys."
                
                m "I looked at Damion with a subtle smile on my face."
                
                if jz_bkevin_chase_join:
                    c "He can teleport around, repair objects with his mind, pass through solid objects and probably a lot more."
                else:
                    c "He can teleport around, repair objects with his mind and probably a lot more."
                    
                c "Why do you think he would be scared of the police?"
                
                Dm "Don't worry, I'll think of something. You'll both regret doing this."
                
                BKv brow flip "Yeah, nice story. I'm so scared right now that you wouldn't even believe how scared I am."
                BKv "I'm so scared that you could chop my scaredness with a frying pan."
                
                BKv smile flip "Anyway, I have to go now!"
                BKv "See ya later, [player_name]."
                
                c "Huh, what do you mean by th-{nw}"
                
                stop music fadeout 1.0
                
                scene testingroom
                show damion normal at right
                with squares
                
                $ renpy.pause(1.0)
                
                play music "mx/elegant.ogg" fadein 1.0
                
                m "Everything around us suddenly changed again and we were now standing back inside the lab."
                m "I looked around the room and noticed that Blueberry Kevin was nowhere to be seen."
                
                c "Uhm, where did he go?"
                
                m "I noticed that the door was still broken, and when looking at the table I saw the shattered pieces of Damion's microscope lying there."
                
                Dm face "I don't really care where he went..."
                Dm arrogant "But don't forget that I'll make you two pay for this eventually."
                
                c "You don't have to make a big deal out of this. I'm sure we could come to-{nw}"
                
            elif jz_bkevin_swimgame_result == "damion":
                $ jz_bkevin_bkev_points -= 1
                
                Dm arrogant "Well, who's the slow sloppy fish now?"
                
                BKv ramble flip "You are. Nothing changed in that regard."
                
                Dm "Hm, maybe you don't want to admit it, but I won our race, as expected."
                
                BKv smile flip "You sure did, congratulations."
                BKv ramble flip "You proved that you're faster than a non-fish, but it still doesn't meant you aren't slow among fish."
                
                Dm face "Whatever, I don't care."
                Dm arrogant "I'm just waiting for you to do what you promised."
                
                BKv think flip "Did I promise something?"
                
                c "He won your bet, so I guess you should repair his microscope again."
                
                Dm "And the door as well."
                
                BKv smile flip "Riiight, how could I forgot such an {i}important{/i} thing."
                
                $ renpy.pause(1.0)
                
                Dm normal "..."
                Dm "I'm waiting."
                
                BKv normal flip "Yeah, yeah, I'll do it."
                BKv "See ya later, [player_name]."
                
                c "Huh, what do you mean by th-{nw}"
                
                stop music fadeout 1.0
                
                scene testingroom
                show damion normal at right
                with squares
                
                $ renpy.pause(1.0)
                
                play music "mx/elegant.ogg" fadein 1.0
                
                m "Everything around us suddenly changed again and we were now standing back inside the lab."
                m "I looked around the room and noticed that Blueberry Kevin was nowhere to be seen."
                
                c "Uhm, where did he go?"
                
                m "I also realized that the door was in one piece again, even though it had broken when he ran through it."
                m "When I looked at the table I saw the microscope sitting there, perfectly fine."
                
                Dm face "I don't really care where he went, but at least he did what he promised."
                
                c "True. Honestly, I didn't expected him to."
                c "Anyway, now that he's gone, could I ask you few questions regarding Reza?"
                
            else:
                c "So? Who is the winner?"
                
                Dm face "Well, it seems like there is none."
                
                c "What does that mean?"
                
                BKv ramble flip "Apparently our race ended in a tie, because the fishboy here is too slow to outswim me."
                BKv brow flip "This is the most boring result our race could have had. I guess someone didn't understand how to play the game properly."
                BKv think flip "Or did this just out of curiosity."
                
                c "You're confusing me again..."
                c "Game? Do you mean your race?"
                
                BKv ramble flip "Well, no, but yes."
                BKv "Doesn't matter. The important thing is, what's the result of our bet?"
                
                Dm arrogant "It's simple. You said you'll fix the microscope if I win."
                Dm "And we both won, therfore I won too, meaning you should go fix it right now."
                
                BKv think flip "Yes, we both won, but we also both lost."
                BKv smile flip "So I should both fix and not fix it."
                
                Dm face "What? That doesn't even make sense, you can't have a thing simultaneously repaired and not repaired."
                
                BKv "Of course you can!"
                
                c "I really want to see how you plan to do it."
                
                $ renpy.pause(1.0)
                
                Dm normal "..."
                Dm "I'm waiting."
                
                BKv normal flip "Yeah, yeah, I'll do it."
                BKv "See ya later, [player_name]."
                
                c "Huh, what do you mean by th-{nw}"
                
                stop music fadeout 1.0
                
                scene testingroom
                show damion normal at right
                with squares
                
                $ renpy.pause(1.0)
                
                play music "mx/elegant.ogg" fadein 1.0
                
                m "Everything around us suddenly changed again and we were now standing back inside the lab."
                m "I looked around the room and noticed that Blueberry Kevin was nowhere to be seen."
                
                c "Uhm, where did he go?"
                
                m "I also realized that the door was in one piece again, even though it has broken when he ran through it."
                m "When I looked at the table I saw the microscope sitting there, perfectly fine."
                
                c "It seems like in the end he decided to repair your stuff anyway."
                c "I'm not sure what he meant by \"both fix and not fix\" then."
                
                Dm face "Me neither, but I don't really care, at least I don't have to get a new microscope and door."
                
                c "Fair."
                c "Anyway, now that he's gone, could I ask you few questions regarding Reza?"
            
            Dm arrogant "No."
                
            c "No?"
                
            Dm "Are you deaf? Leave right now, or I'll kick you out myself."
            Dm "And close the door on your way out please."
            
            if jz_bkevin_swimgame_result == "bkevin":
                c "In the state it's in, there's no way I'm closing that door on my way out."
                Dm face "Just... get out of here."
            else:
                c "Uhm, I guess I should've expected that... Goodbye."
                
            scene black with dissolve
                
            m "I headed out of the room and towards the exit of the production facility."
            m "Even though I was a bit angry that Damion had kicked me out, I could understand why he did so."
            m "Although he could've been more friendly in the first place..."
            
            scene fac1 with dissolve
            
            m "When I got outside I looked around, hoping to see Blueberry Kevin somewhere nearby."
            m "However, he was nowhere to be seen. After some time I decided to head back to the police station."
            
        "You're just being a jerk." if jz_bkevin_bkev_points >= 1:
            
            Dm "Oh, so {i}I'm{/i} a jerk now, but what you two did was completely fine, is that so?"
            
            c "I'm not saying I agree with what he did, but dragging the police into this isn't necessary."
            
            Dr "..."
            
            BKv "Indeed, you're nothing more than an incompetent coward."
            
            Dm normal error "Can you shut up for a moment?"
            
            BKv "Now you're just mad at me for pointing out the truth."
            
            show damion normal error at left with ease
            
            $ renpy.pause(0.5)
            
            Dm "This is for the broken door."
            
            play sound "fx/hit2.ogg"
            
            $ renpy.pause(0.5)
            
            m "Damion walked closer to Blueberry Kevin and gave him a strong kick with his foot."
            
            Dm "This for the microscope."
            
            play sound "fx/hit2.ogg"
            
            Dr "..."
            
            $ renpy.pause(0.5)
            
            Dm "And this for being an asshole."
            
            play sound "fx/wooshimpact2.ogg"
            
            $ renpy.pause(0.5)
            
            show damion normal error behind bkevin at Position(xpos=0.15, ypos=1.6):
                rotate 90
            
            $ renpy.pause(1.0)
            
            m "During the last kick Damion's foot went right through Blueberry Kevin's body like it wasn't even there, making him slip and fall on the ground."
            
            Dm "Ughh... what just happened?"
            
            if jz_bkevin_chase_join:
                c "I think you passed through him like I did before."
                
                Dm "Should've seen that comming..."
            else:
                c "Ehm, you... slipped?"
                
                Dm "Very funny. My foot {i}somehow{/i} went through his body, like I just kicked air."
            
            $ renpy.pause(0.5)
            
            hide bkevin
            show bkevin normal coat flip at left
            with None
            
            $ renpy.pause(0.5)
            
            BKv ramble coat flip "Your kicking privileges have been revoked."
            BKv "I noticed you had a lot of fun kicking me, but I wanted you to enjoy this marvelous ground from up close too."
            BKv smile coat flip "Nice and clean, isn't it?"
            
            Dr "..."
            
            c "It seemed like his kicks didn't even hurt you."
            
            BKv ramble coat flip "Feeling pain hurts, so why would I feel someting that hurts? Would be a very dumb idea if you ask me."
            
            c "If only it was this easy for everyone..."
            
            show bkevin normal coat flip at left with dissolve
            
            $ renpy.pause(0.5)
            
            hide damion
            show damion normal error
            with None
            
            $ renpy.pause(0.5)
            
            m "Damion got up and looked into Blueberry Kevin's face."
            
            Dm "Give me my coat back!"
            
            show damion at Position(xpos=0.3, ypos=1.0) with ease
            hide bkevin
            show bkevin normal coat at right
            with Pixellate(time=0.5, steps=5)
            
            Dm "What the..."
            
            Dr "..."
            
            show damion normal error flip
            
            m "When Damion went after him, Blueberry Kevin suddenly disappeared and reappeared behind him."
            
            show damion at Position(xpos=0.7, ypos=1.0) with ease
            hide bkevin
            show bkevin normal coat flip at left
            with Pixellate(time=0.5, steps=5)
            
            show damion normal error
            Dm arrogant error "Okay, start explaining. How the hell are you doing this?"
            
            c "I'm very curious too. You seem to be able to do things I never thought could even be possible."
            
            BKv smile coat flip "Nothing is impossible, just very improbable."
            BKv ramble coat flip "For example, chances of [player_name] saying the words \"tree\" and \"cute\" in the same sentence during the next few seconds are slim, but never zero."
            
            c "They're exactly zero, because I'm not going to say those words now that I know what they are."
            
            BKv normal coat flip "I wouldn't be so sure about it."
            
            Dr "..."
            
            c "I'm certain. It's not like you can control what I'm saying."
            c "Anyway, doesn't the pink tree over there look kinda cute?"
            
            BKv smile coat flip "Hehe, told ya."
            
            m "I quickly covered my mouth with my hand, unwilling to believe that I had said that. The words slipped out of my mouth on their own."
            
            c "H-how did you do that?"
            
            BKv ramble coat flip "Magicians and politicians never tell their secrets."
            
            Dm "Am I really supposed to believe he made you say those words? It's apparent you two prepared this beforehand. I'm not that dumb."
            
            c "Seriously? He was just teleporting around here and you don't believe he could do something like this?"
            
            Dm "I won't believe until I get direct evidence."
            
            c "I'm telling you he somehow put those words to my mouth, isn't that enough evidence?"
            
            Dm normal error "Your words have little to no value to me. Even less than that cute pink tree nearby."
            
            Dr "..."
            
            BKv smile coat flip "I like how you both agree about the cuteness of that tree."
            
            Dm face error "I wasn't planning to say that."
            
            c "...but it just slipped out of your mouth anyway?"
            
            Dm "Something like that."
            Dm arrogant error "Alright, I still hate you but I now believe you did this."
            
            BKv "Thank you. Your hate is appreciated."
            
            Dm "Now, can you use those weird \"powers\" of yours to repair the microscope?"
            
            BKv normal coat flip "Why would I do that?"
            
            Dr "..."
            
            Dm "To prove you're not as big of an asshole as you seem to be so far."
            Dm "If you repair it and give me my coat back, I can forget this ever happened."
            Dm face error "The lab doors shouldn't be too expensive to get repaired."
            
            c "Wait a second. A moment ago you wanted to call the police, but now you've decided to simply sweep it under the carpet?"
            
            Dm normal error "Apparently, there's no other choice. With the powers this guy has, the only option left for me is to reason with him."
            
            c "Fair point."
            
            BKv "Hmmm... this is all very interesting."
            BKv ramble coat flip "You're even more of a coward than I thought."
            BKv normal coat flip "Should I agree to it or not? Hard decision."
            BKv smile coat flip "Wait a second, why not make this a bit interactive? I'm sure you're bored behind your screen from all this linearity already."
            BKv "The decision is yours! Should I reason with this Damion jerk or should I just walk away?"
            
            c "Huh... are you asking me, or?"
            
            BKv normal coat flip "Well, yes but actually no."
            
            Dr "..."
            
            BKv "You wouldn't understand."
            
            c "Okay?"
            
            show bkevin smile coat flip with dissolve
            
            menu:
                BKv "Anyway, what's your choice?"
                "Repair his microscope, he's suffered enough.":
                    BKv normal coat flip "Well, if you want me to..."
                    
                    c "Are you seriously going to do it?"
                    
                    BKv "Of course."
                    
                    Dr "..."
                    
                    $ renpy.pause(2.0)
                    
                    Dm arrogant error "Well? I'm waiting."
                    
                    BKv "If you actually paid attention to what was going on, instead of dreaming about hot raptor girls, you'd have noticed that the microscope is already repaired."
                    
                    m "Both Damion and I looked at the microscope."
                    m "The pieces weren't there anymore. Instead, the small microscope was in one piece again, looking exactly the same as when he took it from Damion's lab, with one obvious difference..."
                    
                    Dm face error "Why is it pink now?"
                    
                    BKv ramble coat flip "Pink is a wonderful color. Not as great as blue, but still more creative than green."
                    
                    Dm normal error "Not for me. The color it had before was just fine."
                    
                    Dr "..."
                    
                    BKv smile coat flip "Too bad. Pink microscope or broken microscope, there's no other choice."
                    
                    Dm face error "Whatever... I can paint it myself I guess."
                    Dm arrogant error "Now the coat. And if it turns pink, I'm going to slap you with a branch from that cute tree."
                    Dm normal error "Ugh..."
                    
                    BKv "Sorry, couldn't resist."
                    BKv normal coat flip "Here comes the coat."
                    
                    show bkevin normal flip at left
                    show damion normal at Position(xpos=0.7, ypos=1.0)
                    with squares
            
                    $ renpy.pause(0.5)
            
                    BKv ramble flip "Well, are you happy now?"
                    BKv smile flip "It didn't get damaged in any way, so you can keep doing happy coat stuff in it."
                    BKv "And you can do happy microscope stuff too!"
                    BKv "Isn't this a wonderful day?"
                    
                    Dm "It will only start to get better when I'm certain I'll never see you again."
                    
                    BKv normal flip "Your wish is my command."
                    
                    Dm "Good."
                    
                    hide damion with dissolve
                    
                    m "Damion picked up his repaired pink microscope from the ground and gave us both a piercing look."
                    
                    show damion normal at Position(xpos=0.7, ypos=1.0) with dissolve
                    
                    Dm "Goodbye."
                    
                    c "Bye."
                    
                    Dr "..."
                    
                    BKv smile flip "See ya!"
                    
                    Dm "Oh, I really hope I won't."
                    
                    show damion normal flip with None
                    
                    $ renpy.pause(0.2)
                    
                    hide damion with easeoutright
                    show dramavian normal flip at Position(xpos=0.7, ypos=0.5) with None
                    
                    $ renpy.pause(0.2)
                    
                    hide dramavian with easeoutright
                    
                    $ renpy.pause(0.5)
                    
                    c "(Huh? Why is the orange dragon following him?)"
                    
                    m "I kept watching Damion as he walked away with his now pink microscope."
                    m "After he was out of sight, I turned back to Blueberry Kevin."
                    
                    c "I'm glad you repaired his microscope. He may a bit of a jerk, but that was still the right thing to do."
                    
                    BKv ramble flip "You cannot do a kindness too late because you never know how late it will be too soon."
                    BKv think flip "I think I got that a bit wrong, but you get the idea."
                    
                    c "Huh, I think it should be the other way around, but whatever."
                    
                "Nah, screw this guy.":
                    BKv "If you say so."
                    
                    c "I do, he doesn't deserve us helping him with anything."
                    
                    Dr "..."
                    
                    BKv normal coat flip "If that's your choice, then there's nothing more here for me to do."
                    BKv smile coat flip "See ya, fishhead!"
                    
                    show bkevin normal coat with None
                    
                    $ renpy.pause(0.2)
                    
                    hide bkevin with easeoutleft
                    
                    $ renpy.pause(1.0)
                    
                    m "Blueberry Kevin casually walked away without once glancing back at us."
                    
                    Dm arrogant error "I will remember this, [player_name]."
                    
                    c "Uhm, sorry, I think I have to go now."
                    
                    Dm "Ah, sure, run away like your dumb friend did. This is far from over, you'll see me again and you won't enjoy it."
                    
                    Dr "..."
                    
                    c "Is this a threat?"
                    
                    Dm "Are you deaf, stupid or both? Of course it is."
                    
                    c "I'd {i}love{/i} to talk with you more about it, but I really need to go."
                    
                    Dm "Whatever, get lost."
                    
                    $ renpy.pause(0.5)
                    
                    scene town2 with wipeleft
                    
                    m "I started running towards Blueberry Kevin, catching up with the surprisingly fast furred dargon."
                    
                    show bkevin normal flip with dissolve
                    
                    c "Hey, wait for me!"
                    c "I didn't expect you to just walk away like that."
                    
                    BKv think flip "There was nothing else to talk about, so I just left. Isn't that how people usually behave?"
                    
                    c "No... maybe... sometimes."
                    c "They can, but it's somewhat rude to just walk away like that."
                    
                    BKv normal flip "So I'm a rude person I guess."
                    BKv think flip "Therefore I should say something rude to you for no reason."
                    BKv ramble flip "Your hamster was a mother and your elderberries smelt of a father!"
                    BKv normal flip "Now I'm a proper rude person."
                    
                    c "..."
                    c "You're really random sometimes."
                    c "Wait, what am I saying. Not sometimes, you're always like this."
                    
                    BKv smile flip "Thank you!"
                    BKv think flip "Wait, that's not rude."
                    BKv smile flip "Thanks, you food-headed empty-animal-trough wiper!"
                    
                    m "I had no idea how to react so I just stayed quiet."
                    m "Suddenly I realized that there was something different about him."
                    
                    c "Wait, I thought you had Damion's coat on you."
                    
                    BKv normal flip "I gave it back to him. I didn't like it."
                    BKv "Clothes are overrated anyways. I prefer not to wear any."
                    
                    c "When did that happen?"
                    
                    BKv ramble flip "Lightning flashes, sparks shower, in one blink of an eye, you have missed seeing."
                    
                    c "I'm not sure if I understand what's that supposed mean."
                    
                    BKv brow flip "Me neither, it's just a random quote from somewhere."
                
            jump jz_bkevin_chap2facility_bye1
            
        "Maybe there's another solution.":
            Dm "What are you suggesting?"
            
            c "There must be something we could do for you in return for not dragging the police into this."
            
            Dr "..."
            
            Dm "This isn't a negotiation."
            Dm face error "Although, thinking about it, there might be some... favors you could do for me."
            
            BKv "Oh no, keep me out of that!"
            
            Dm normal error "Not {i}that{/i} kind of favor."
            
            c "I really hope so. What is it you want then?"
            
            Dm arrogant error "First I want my coat back. There won't be any more discussion until then."
            
            Dr "..."
            
            BKv "There is no such thing as your coat anymore. Its ownership has been forcefully transfered and it's my coat now."
            
            if jz_bkevin_bkev_points >= 3:
                c "Can you give him his coat back, please? It won't hurt you."
                c "Also I'd appreciate if you weren't lying on the ground again."
            else:
                c "Can you stop inspecting the floor and give him his coat back?"
                
            BKv "Hmmm... but this floor is much cleaner than the one in his lab. They most likely have a very capable cleaning crew in this park."
            
            c "..."
            
            BKv "Hey, stop stealing the small guy's lines!"
            
            c "What?"
            
            BKv "Nothing."
            BKv "I'm getting up now."
            
            c "Sometimes I have no idea what you're even talking about..."
            
            BKv "\"Getting up\" means to change your position to a more upright one. For example lying to sitting or sitting to standing. Basic phrases."
            
            c "..."
            
            BKv "You did it again!"
            
            Dm normal error "Give me my coat or I'm gonna change your position to a beaten up one."
            
            BKv "No need for more threats, I'm getting up."
            
            hide bkevin with dissolve
            show bkevin normal coat flip at left with dissolve
            
            c "Thank you."
            
            show bkevin normal flip at left
            show damion normal at right
            with squares
            
            $ renpy.pause(0.5)
            
            BKv ramble flip "Well, are you happy now?"
            BKv smile flip "It didn't get damaged in any way, so you can keep doing happy coat stuff in it."
            BKv think flip "However, you probably won't be able to do happy microscope stuff anymore."
            BKv ramble flip "But that's nothing we can't fix with a bit of flex tape!"
            
            Dm normal "If I get my hands on a flex tape, your mouth will be the first place for it to stick to."
            
            Dr "..."
            
            c "Calm down you two."
            c "Damion, you have your coat back now, so you can tell us what favor we can do for you."
            
            show bkevin normal flip with dissolve
            
            Dm arrogant "Considering you were asking for Anna, I see that you already know her well."
            
            if annastatus == "bad":
                c "I do, but I it's not like we're friends or anything. I met with her the other day, and it didn't go too well."
                
                Dm face "She's a bitch, isn't she? Now imagine having to share a lab with someone who still throws temper tantrums like a toddler."
                
                c "Anyway, what does it have to do with her?"
            else:
                c "I met her once or twice already. What does it have to do with her?"
                
            Dr "..."
            
            Dm normal "There's one matter regarding her which I could use your help with."
            
            c "Just spit it out."
            
            Dm face "Not yours. You're kinda... attracting too much attention to yourself, because of being a human and all."
            Dm arrogant "I meant your blue companion here, who's in danger of getting punished for vandalism."
            
            BKv think flip "Who's that guy? Sounds like he's in a bit of a trouble."
            
            c "I think he meant you..."
            
            BKv smile flip "Oh! So you need my help? You'll see that I'm very good at helping."
            BKv think flip "I'm not sure why I should even be helping you though."
            
            Dr "..."
            
            Dm "I don't think you have any other choice."
            Dm face "Can you let us speak in private, [player_name]?"
            
            BKv smile flip "I'm gonna hear some condifential stuff? How exciting."
            
            c "Huh? You want me to leave?"
            
            Dm "Yes. Do it, before I change my mind."
            
            c "Fine, I'm going."
            
            scene town2 with wipeleft
            
            m "I started walking away from them towards the center of the park. When I was a fair distance away, I stopped and turned towards them, waiting until they finish talking."
            m "Damion was speaking very quietly and I could see him rolling his eyes quite often, while looking unamused."
            m "Even though Blueberry Kevin's voice was a bit louder, I still couldn't recognize most of his words. He was smiling and waving his arms around when talking, as he always does."
            m "While watching them I realized that the orange dragon was still sitting there and listening. Neither of them seemed to mind."
            m "After few minutes, Blueberry Kevin started quickly walking towards me and I could hear Damion calling at him."
            
            Dm "Remember, if you refuse to do it, you won't enjoy the consequences!"
            
            $ renpy.pause(1.0)
            
            show bkevin normal flip with dissolve
            
            BKv "Thanks for waiting for me."
            
            c "No problem. What did he want from you?"
            
            BKv ramble flip "I can't tell you that."
            
            c "Come on, I can see that he doesn't want me to know it, but since when do you care about what he says?"
            
            BKv "Because a) I made a promise I won't tell anyone and b) I wasn't really paying attention to him much."
            
            c "Huh. So you're actually thinking about doing it?"
            
            BKv smile flip "Of course! It sounded like it could be fun, at least for the first 10 seconds when I was listening."
            
            c "How are you going to do what he wanted when you weren't listening to it?"
            
            BKv ramble flip "Using my imagination to fill the blanks of course! He'll be happy after seeing what I did!"
            
            menu:
                "Haha, I'm sure he will.":
                    $ jz_bkevin_bkev_points += 1
                    
                    BKv smile flip "See? It's gonna be fun."
                    
                    if jz_bkevin_bkev_points >= 3:
                        c "Can I be a part of it?"
                    
                        BKv normal flip "Of couse, I'll let you know. It's much more fun with you around."
                    else:
                        c "I'd like to see his reaction to whatever you do."
                        
                        BKv normal flip "You'll have a chance to, don't worry."
                        
                    c "Looking forward to it!"
                    
                "This was a bad idea...":
                    BKv normal flip "Don't worry, I can handle it."
                    
                    c "Don't whatever you want, but I don't want to be a part of it."
                    
                    BKv "You're gonna miss on a lot of fun."
                    
                    c "I can live with that."
            
            jump jz_bkevin_chap2facility_bye1
            
        "I agree with you, we should take him to the police station." if jz_bkevin_bkev_points <= 0:
            $ jz_bkevin_bkev_points -= 1
            
            Dm "I'm glad you agree with me, [player_name]. He may be your friend, but letting him do illegal things would not look good for you."
            
            c "He isn't really my friend, just kind of sticks around. I only met him yesterday."
            
            Dm face error "Taking a random guy on an investigation with you doesn't sound like a good idea. Maybe they should've chosen someone more capable than you for it."
            
            if jz_bkevin_chase_join:
                Dr "..."
                
                Dm "However, what's stopping him from passing through you as he did before?"
                
                c "I'm not sure why, but he didn't pass through this orange guy here and fell over him instead."
                
                Dm "Maybe he has to concentrate to use that ability and this time it happened too fast for him to react."
                Dm arrogant error "Or he did it on purpose to \"accidentally\" break the microscope."
                
                c "I can't believe we're talking about this as if it's something normal."
                
                Dr "..."
                
                Dm arrogant error "I'm a scientist, my view of reality isn't based on our believes or emotions. It's based on observations, experiments and scientific proofs."
                Dm "I saw him pass right through you, so I can't deny the fact it happened."
                Dm "So let's get over the suprise and think about what to do now."
                
                c "We can try to bring him to the police and see what happens."
                
                Dm "Yes, like we have any other option..."
            
            else:
                c "..."
                c "Let's take him and go to the police. I want Bryce to see him anyway."
            
            Dr "..."
            
            hide bkevin
            hide damion
            with dissolve
            
            $ renpy.pause(0.5)
            
            m "We both approached Blueberry Kevin and helped him stand up. Damion took the coat off him and put it on himself, while I picked up the broken pieces of the microscope. Then we both grabbed of the blue dragon's forearms and headed out of the park."
            m "The small orange dragon sat in the same spot, watching us while not saying a word. I still had no idea who they even were."
            
            Dr "Refulshtacking."
            
            scene town2 with wipeleft
            
            m "Blueberry Kevin stayed quiet for the while we brought him to the police station. He didn't offer any resistance and went with us calmly."
            m "...too calmly."
            
            c "Why are you not talking?"
            
            BKv "..."
            
            m "He looked at me with an empty face and then turned his head away again."
            
            Dm "He either feels guilty or is angry at you for betraying him."
            Dm "Doesn't matter, we should keep going."
            
            scene black with dissolveslow

            $ renpy.pause (1.0)

            scene office at Pan ((128, 250), (0, 250), 3.0) with dissolveslow

            $ renpy.pause (1.0)
            
            m "We arrived at the office, but there didn't seem to be anyone inside. At least not in the main room."
            
            show bkevin none flip at left
            show damion arrogant at right
            with dissolve
            
            Dm "Alright [player_name], I'll stay here and you go look around for someone."
            
            c "No, you go. I'll keep an eye on him."
            
            Dm face "Whatever, just don't try to run away."
            
            c "We won't."
            
            show damion arrogant flip
            
            $ renpy.pause(0.2)
            
            hide damion with easeoutright
            
            m "He left the room and I stayed there alone with the unusual dragon, who still refrained from saying a word."
            
            c "You always have something to say, why are you quiet now?"
            
            BKv "..."
            
            c "Are you angry at me? I'm sorry, but you can't just go somewhere and destroy people's property."
            
            BKv "..."
            
            m "He stared at me blankly, his eyes gazing at some far off thing and his mouth unmoving. It was starting to get a bit creepy."
            
            c "What's wrong with you?"
            
            BKv "..."
            
            m "After few moments Damion came back with Sebastian following closely behind him."
            
            show damion normal at Position(xpos=0.9, ypos=1.0) with easeinright
            show sebastian normal b behind damion at right with easeinright
            
            Sb "Hello, [player_name]."
            Sb "Sorry for the wait, I was just taking a shower."
            
            c "Oh, you didn't have to hurry, we're in no rush."
            
            BKv "..."
            
            Sb smile b "Not a problem, I was already done and met Damion in the corridor."
            Sb normal b "Now, explain me what's going on here. This guy is the vandal who broke into your lab?"
            
            Dm arrogant "Correct. He ran through the door, then took an expensive microscope and ran away."
            
            Sb disapproval b "Interesting..."
            Sb normal b "Where's the microscope now?"
            
            m "I showed Sebastian the broken pieces I had picked up from the ground."
            
            Sb "How did that happen?"
            
            Dm "He fell on the ground while running, which made it shatter into pieces."
            
            c "That's how we managed to catch him."
            
            Sb disapproval b "I see..."
            Sb normal b "Let's hear what he has to say about it."
            
            BKv "..."
            
            Sb "Sir? I'm talking to you."
            
            m "He looked at Sebastian without even moving an eyebrow."
            
            Sb drop b "Does he not talk?"
            
            c "Ehm... for some reason he stopped talking a while ago."
            
            Sb normal b "So he {i}did{/i} talk before?"
            
            Dm "Way too much, I'd say."
            
            c "Yes, he haven't said a word since... well... since he hit the ground and broke the microscope."
            
            Sb drop b "Wait, you're saying he's been acting like this since he hit his head on the ground?"
            Sb "We should take him to the hospital, this doesn't sound good."
            
            BKv "..."
            
            Dm face "I thought about it being caused by the fall, but I'm convinced he's just faking this."
            
            m "I turned my head towards Blueberry Kevin and kept looking into his eyes."
            
            c "It's not funny, if you're really just playing it, tell us."
            
            BKv "..."
            
            hide sebastian
            hide damion
            hide bkevin
            with dissolve
            
            nvl clear
            window show
            stop music fadeout 1.0
            n "He didn't tell us anything."
            n "Sebastian and Damion both spent the next few minutes trying to get any reaction from him."
            n "I spent the time thinking about what could be the cause of his silence."
            n "There was definitely something weird going on here. He didn't just lose the ability to speak. He seemed empty, like if his soul had left his body completely."
            n "Damaging ones brain by hitting the ground with their head is something which would happen to some normal person. However, even after the short time I spent with Blueberry Kevin, I understood that he wasn't a \"normal person\" at all."
            nvl clear
            n "He was the complete opposite. A dragon who appeared out of nowhere with almost supernatural abilities and a weirdly chaotic personality."
            n "I was trying to remember how he appeared. First he was just a wooden blueberry and then he {i}somehow{/i} made himself a body... and..."
            play music "mx/slowpiano.ogg"
            n "Oh!"
            n "He made himself a body and {i}transfered{/i} his consciousness into said body."
            n "If he was able to move his consciousness from one place to another once, there's the possibilty that-"
            window hide
            nvl clear
            
            show bkevin none flip at left
            show sebastian normal b at right
            show damion normal at Position(xpos=0.9, ypos=1.0)
            with dissolve
            
            c "Wait a minute!"
            
            m "I was sure I managed to figure out what happened, but I had no idea how to explain it to Sebastian and Damion."
            
            c "He can't answer because his consciousness isn't in his body anymore. It's just an empty shell."
            
            Dm face "What? That sounds too crazy even to me."
            
            c "Just look at him. We're trying to get a response, but we can't because nobody is home."
            
            BKv "..."
            
            Sb drop b "I'm not sure if that's even possible."
            
            c "Everything is possible with this guy."
            
            Dm face "I mean... you're right about him acting like his mind is gone. But a mind isn't something you can easily take out and put elsewhere."
            Dm arrogant "And even if it could, it couldn't just disappear. So tell me where it went according to your amazing theory."
            
            c "I'm... not sure."
            
            Dm "Then your theory sucks."
            
            BKv "..."
            
            Sb disapproval b "I wouldn't use the same words, but I agree with Damion. It seems like too much of a stretch."
            Sb normal b "He doesn't look like his mind is gone, he looks like there's something wrong with his brain."
            
            c "There must be some place that his mind could've gone to..."
            
            Dm face "Well, actually... there may be one."
            
            c "What are you hinting at?"
            
            Dm "Remember the small orange guy? They didn't say a word the whole time we were in the park."
            
            c "Yes...?"
            c "Wait, so you believe he moved his consciousness into that dragon?"
            
            BKv "..."
            
            Dm face "Not moved... switched."
            Dm "I agree with the confused police guy here, he acts like he has some kind of disorder."
            
            Sb drop b "I appreciate that, but does either of you even realize how insane this idea is?"
            
            c "Maybe, but over my time here I'm slowly getting used to \"insane\"."
            
            Dm arrogant "The thing is... when we were leaving the park I heard the orange guy say something."
            
            c "Really? What did they say?"
            
            Dm face "I don't know, it sounded like gibberish, but maybe we were just too far away."
            
            c "So the orange guy's consciousness is inside Blueberry Kevin and his consciousness is inside the orange guy?"
            
            Dm "That's what it looks like. Doesn't mean it's true."
            
            c "We should get back to the park and find him then."
            c "Although... what if Sebastian is right and we should really send him to the hospital?"
            
            Dm arrogant "It's your decision, I don't want to be responsible for this."
            
            menu:
                c "(What should we do?)"
                "[[Go back to the park.]":
                    c "Let's go back to the park. Hopefully he's still there."
                    
                    Sb disapproval b "Hey!{w} Wait!"
                    
                    scene black with dissolve
                    
                    $ renpy.pause(2.0)
                    
                    scene park2
                    show damion arrogant
                    with dissolve
                    
                    Dm "Well, I don't see any orange dragon anywhere around here."
                    
                    c "Me neither, if Blueberry Kevin's mind is indeed inside his body, he could be anywhere."
                    
                    Dm "He most likely isn't in the park anymore."
                    Dm "By the way, what did you mean by \"everything is possible with this guy\"?"
                    
                    c "That's a... long story."
                    
                    Dm face "It's not like I have any other plans today... well... not anymore at least."
                    
                    c "Fine, I don't have time to do more investigating today anyway, so I can probably tell you about my first encounter with him."
                    
                    Dm arrogant "I can't wait to hear about it."
                    
                    scene black with dissolve
                    
                    $ renpy.pause (2.0)
                    
                    scene park2
                    show damion face
                    with dissolve
                    
                    Dm "This is even weirder than I expected."
                    
                    c "Yeah, have you ever heard about someone like this? Like maybe some stories and such?"
                    
                    Dm arrogant "Well, there are many stories from people who {i}believe{/i} they saw something paranormal, like a ghost and such, but none of them were ever scientifically proven."
                    Dm "So for me they are just a bunch of crap."
                    
                    c "Nothing about... hm... blueberries?"
                    
                    Dm face "No, I've never heard about a sentient blueberry taking the form of a dragon."
                    Dm "If I didn't see him myself, I'd think you're insane."
                    Dm arrogant "Which you still might be..."
                    
                    c "To be honest, I'm not sure myself."
                    c "Well, I guess there's nothing more we can do right now, so I should return to the police office."
                    
                    Dm "I'm gonna head back to the lab. I'd say it was nice meeting you, but I don't want to lie."
                    Dm face "However, if you get to know something more about this... Blueberry Kevin, let me know."
                    Dm "I could run some... experiments on him to determine who or what he is."
                    
                    c "Hm, maybe, no promises."
                    c "Goodbye."
                    
                    Dm arrogant "Bye."
                    
                    scene black with dissolveslow
                    stop music fadeout 1.0
                    $ renpy.pause(2.0)
                    scene office at Pan ((128, 250), (0, 250), 3.0) with dissolveslow
                    $ renpy.pause (1.0)
                    
                    show sebastian normal b with dissolve
                    play music "mx/barren.ogg" fadein 1.0
                    
                    Sb "I'm glad you came back."
                    Sb "So, did you find that... other guy who he supposedly {i}switched{/i} bodies with or whatever?"
                    
                    c "No. We searched the park and haven't found anyone. He must've ran away already."
                    c "Wait..."
                    
                    $ renpy.pause(1.0)
                    
                    c "Where's he?"
                    
                    Sb drop b "I sent him to the hospital."
                    
                    c "Why would you do that?"
                    
                    Sb disapproval b "Because I was sure it was the best thing to do. I really don't believe any of that mind switching madness you were talking about."
                    
                    c "But..."
                    
                    Sb "I'm sorry, [player_name]."
                    
                    c "Fair... I guess I'd do the same if I were you."
                    
                    Sb normal b "I'm glad you agree."
                    
                "[[Send him to the hospital.]":
                    c "Let's do what Sebastian is suggesting and send him to the hospital."
                    
                    Sb smile b "Good decision."
                    
                    hide sebastian
                    hide damion
                    hide bkevin
                    with dissolve
                    
                    nvl clear
                    window show
                    stop music fadeout 1.0
                    n "Sebastian called an ambulance, which picked up Blueberry Kevin and took him to the hospital."
                    n "He didn't say a word during the whole thing."
                    n "Even though I still believed in my theory, I was happy with my decision to listen to Sebastian and not to my crazy ideas."
                    n "I didn't like Blueberry Kevin's behaviour, but despite that, I wished for him to be alright."
                    nvl clear
                    n "Damion left us and headed back to the production facility. He didn't look happy at all and I had a feeling I was going to see him again soon."
                    n "After all of that, Sebastian and I stayed alone at the police office."
                    window hide
                    nvl clear
            
                    $ renpy.pause(0.5)
                    
                    show sebastian normal b with dissolve
                    play music "mx/barren.ogg" fadein 1.0
                    
            jump jz_bkevin_chap2facility_seb2
    
    jump jz_bkevin_chap2facility_preseb


label jz_bkevin_chap2facility_bye1:
    BKv normal flip "Now, what's our next step Mr. Investigator?"
                
    c "Not much, it's getting late and there isn't much time to visit another place from the list."
    c "We should probably return to the police station. Sebastian most likely won't be happy, but at least we tried."
                
    BKv smile flip "Don't worry, I'm sure you'll do a great job on your next investigation."
                
    c "I may, but I'm not as sure about you..."
                
    BKv normal flip "Hey, talking with that grumpy guy would be much less fun without me."
                
    c "Hmm..."
    c "Let's just get going."
                
    BKv "I'm gonna go now, you can handle that visit on your own."
    BKv smile flip "See ya!"
                
    c "Alright th-{nw}"
                
    hide bkevin with squares
                
    $ renpy.pause(0.5)
                
    m "Before I even finished saying goodbye, he vanished into thin air."
    m "I stood there for a moment, thinking about what a strange guy he was before heading to the police office."


label jz_bkevin_chap2facility_preseb:
    scene black with dissolveslow
    stop music fadeout 1.0
    $ renpy.pause(2.0)
    scene office at Pan ((128, 250), (0, 250), 3.0) with dissolveslow
    $ renpy.pause (1.0)
    
    jump jz_bkevin_chap2seb

label jz_bkevin_chap2facility_seb1:
    c "Well..."
    
    Sb "Hm? You did some investigating as we told you to do, right?"
    
    c "I tried, but... it didn't go very well."
    
    Sb "Why so?"
    
    c "I went to the production facility to ask Anna some questions."
    
    Sb "Yes, that was one of the places on the list. Did you run into some problems?"
    
    c "Kinda."
    
    Sb drop b "Can you please be a little more specific? Did you manage to question her or not?"
    
    c "No, because she wasn't there, only her assistant."
    
    Sb normal b "Ah, Damion, I can image he wasn't very cooperative."
    
    c "Exactly."
    
    Sb "Don't worry, it's never an easy talk with that guy. I'd think you would be able to manage it anyway, but maybe I'm putting too much hope into your abilites."
    Sb "Have you been to any other place from the list?"
    
    c "No, I haven't."
    
    Sb disapproval b "So over the last several hours you only managed to visit Damion, who didn't tell you anything and that's all?"
    
    m "I stopped for a moment, thinking if I should tell him about Blueberry Kevin, but decided it would be better to keep it to myself. For now."
    
    c "Yeah, that's all. Sorry."
    
    Sb "Ugh, I'll be trying to give you some simple tasks and not have you investigating on your own, because it's apparently not your strong suit."
    Sb normal b "The rest of the police force and I will have to check things out ourselves to see if we can find more details."
    Sb "In any case, thank you for your help. Or at least attempting to help. We really do appreciate it."
    
    m "I felt a bit offended by Sebastian when he talked to me like I was completely useless, but I couldn't be angry at him for it, because I realized that from his perspective, it may actually seem like that."
    
    jump jz_bkevin_chap2facility_seb_end

label jz_bkevin_chap2facility_seb2:
    Sb "Now that everything is dealt with I have to ask, how did your investigation go today? Find anything important?"
    
    c "Well..."
    
    Sb "Hm? You did some investigating as we told you to do, right?"
    
    c "I tried, but... it didn't go very well. Although you already know what happened."
    
    Sb "Indeed. You made the right choice to bring that guy here. We can have some other people do the investigation instead, even though we'll lose some time by doing so."
    Sb "I understand that you haven't been to any other places from the list?"
    
    c "No, I haven't."
    
    Sb "Hmmm..."
    Sb "In any case, thanks for your help. We really do appreciate it."
    
    m "I stopped for a moment. My mind was very confused about what happened. Would I ever see Blueberry Kevin again?"
    
    jump jz_bkevin_chap2facility_seb_end
