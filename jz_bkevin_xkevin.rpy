label jz_bkevin_xkevin_choice:
    $ renpy.pause (0.5)
    
    stop music
    play sound "fx/system.wav"
    
    s "..."
    s "Wrong scene again."
    s "Seems like the program caused more damage than I thought. The option to meet with Kevin should appear only in a later chapter."
    s "I'm gonna take you back to the character select menu."
    s "Although..."
    
    $ renpy.pause (0.5)
    s "...meeting with Kevin doesn't rely on the main story, so it won't be a problem to go through it now."
    s "Also more random jumping between scenes could cause your character to eventually go insane, so I'd rather not jump away from this."
    s "And if I'm correct, the blueberry is still stuck somewhere, meaning it won't bother us."
    s "Do you want to continue the meeting with Kevin?"
    
    menu:
        "Yes.":
            $ jz_bkevin_bkev_points += 1
            $ jz_bkevin_kevin_talk = True
            s "Alright."
            s "Enjoy the talk with college dragon."
            
            $ renpy.pause (1.0)
    
            play music "mx/dash.ogg" fadein 2.0
    
            jump jz_bkevin_xkevin_choice_return
            
        "No.":
            $ jz_bkevin_kevin_talk = False
            s "Alright, let's jump to the character selection."
            s "Hopefully your character is gonna be fine."
            
            stop music fadeout 1.0
            $ renpy.pause(3.0)
            scene o at Pan((0, 250), (0, 250), 0.1) with dissolvemed
            
            jump jz_bkevin_kevinmeet_menu_return

label jz_bkevin_xkevin_alt:
    if jz_bkevin_appeared:
        jump jz_bkevin_xkevin_alt_return
    
    scene black with dissolveslow
    $ renpy.pause (1.0)
    scene o at Pan((0, 250), (0, 250), 0.1) with dissolvemed
    
    play sound "fx/door/doorbell.wav"
    
    m "I heard the doorbell and went to open the door."
    
    stop sound fadeout 0.5
    $ renpy.pause(0.5)
    show kevin normal with dissolvemed
    play music "mx/dash.ogg" fadein 2.0
    
    Kv "Hello [player_name]. Sorry for coming here out of sudden, but I really need to speak with you."
    
    c "Uhm, did we met?"
    
    Kv brow "You don't remember?"
    
    c "N-no...{w} Well, maybe."
    c "I'm not sure, my memory doesn't work properly since I came to this world."
    c "Kevin, right?"
    
    Kv "Indeed. I believe we met today in the town."
    Kv think "Did you just say you're having some memory issues?"
    
    c "I sometimes have blackouts and forget why I do things."
    c "But I think I remember you. I gave you the wooden blueberry, is that true?"
    
    Kv face "Yes you did, I lost it somewhere on my way here, but that's not important."
    Kv ramble "If you have problems with your memory too, then my assumptions may be true."
    
    c "Hold on. I thought the portal somehow messed with my brain..."
    c "What kind of problems do you have?"
    
    Kv think "My memories since the time you gave me the blueberry until I came to your apartement just now are very blurry and don't make much sense."
    Kv "I believe that I spent some more time on the street, occasionaly talking to someone passing through there to let them know about the college."
    Kv "Also I went to get some food, spent time in the park and lost the blueberry somewhere along the way."
    
    c "Seems like you remember all of it."
    
    Kv ramble "No, I'm convinced this isn't what really happened. I can't recall anything specific and I have some conflicting memories of talking with you in this apartement, but I don't even remember coming here before."
    
    c "Hmmm. Thinking about it, I also feel like you were here before."
    c "Don't remember much, just have a vague memory of you saying that you like cartoons for adults."
    
    Kv face "What?"
    Kv brow "Anyway, I'm sorry, I shouldn't be loosing gifts from people like that."
    
    c "It's fine, I just found it here in the kitchen, so it didn't really have any value to me."
    c "Doesn't matter, as you said, it's not important. It looks like we have bigger problems to care about."
    
    jump jz_bkevin_xkevin_short

label jz_bkevin_xkevin:
    c "By the way, do you still have that blueberry I gave you?"
    
    Kv face "No, I'm afraid I lost it on my way here."
    
    c "Oh, you mean while we were coming back from the portal?"
    
    Kv "No, no, before when I was coming to your apartement to see you."
    Kv brow "I hope you aren't to angry at me for that."
    
    c "No, don't worry about it, things like that do happen sometimes."
    
    Kv normal "Thank you."
    Kv think "I don't remember how exactly I lost it, but it had to happen while I was handing flyers to someone."
    Kv face "I'm sorry, I shouldn't be loosing gifts from people like that, it probably had a lot of value to you."
    
    c "It's fine, really."
    c "The blueberry didn't have any value to me, I found it here in the kitchen."
    
    Kv brow "Okay. Why did you give it to me then?"
    Kv think "Did you really just feel I should have it, or was there more to it?"
    
    c "To be honest, I'm not really sure."
    c "My memory doesn't work properly since I came to this world, I sometimes have blackouts and forget why I do things."
    
    Kv "Doesn't sound good."
    Kv ramble "Could it have something to do with the portal? Maybe the journey through it did some damage to your brain and as a result, you now have problems with your memory."
    
    c "I came to the same conclusion. But there are still things which don't add up."
    
    Kv "Such as?"
    
    c "Reza, the other human who came here through the portal before me, didn't mention anything about him having any memory problems. Isn't it weird that it had negative effects just on me?"
    
    Kv think "Maybe it has an effect only on certain people. Or it could have different negative side effects on him."
    
    c "Hmmm... thinking about it, that would explain a lot."
    
    Kv normal "What do you mean?"
    
    c "I can't really talk about that, sorry."
    
    Kv brow "More secrets, huh?"
    
    c "Don't be mad, it's better for everyone when police keeps these things in secret."
    c "People won't start panicking and won't get in the way of police work."
    
    Kv "Sometimes I wish they got in their way, so they can't do whatever they want without consequences."
    
    c "You don't trust the police departement?"
    
    Kv ramble "I don't trust people with secrets."
    
    c "So you don't trust me either?"
    
    Kv "I barely know you, so until I get to know you better, I have no reason to trust you."
    Kv normal "Although I'm open to you gaining my trust eventually."
    
    c "Makes sense... sounds a bit like paranoia to me, but I agree that naively trusting everyone can be dangerous."
    
    Kv ramble "I'm being skeptical. If I were paranoid I wouldn't even come here."
    Kv normal "It's easy to fall from skepticism into paranoia, but I'm trying my best not to."
    
    c "Fair enough."
    c "However, you still seem to be fine with accepting gifts from strangers."
    
    Kv face "Well, it was just an ordinary wooden blueberry, it's not like it could eat me alive the moment you turned around."
    
    c "Blueberries eating people alive sounds like a cool horror movie idea."
    
    Kv ramble "If by {i}cool movie{/i} you mean {i}a wicked creation somewhat resembling a movie{/i} then I agree with you completely."
    Kv think "On the other hand, it probably wouldn't be the weirdest one I've ever seen."
    Kv "Anyway, to be honest with you, I don't remember why I accepted the blueberry."
    
    c "I'm not the only one who has memory problems then..."
    c "What do you remember after I gave it to you?"
    
    Kv "Hmmmm..."
    
    $ renpy.pause (0.5)
    
    Kv ramble "Well, after you left, I spent some more time on the street, occasionaly talking to someone passing through there to let them know about the college."
    Kv "After that I walked here, while still talking to people walking by and lost the blueberry somewhere on the way."
    
    c "Seems like you remember all of it."
    
    Kv brow "No, this is what my brain thinks did happen, because it makes the most sense to happen."
    
    c "I don't understand what you're trying to say at all right now."
    
    Kv ramble "Scientists don't know yet how exactly our memory works, but it is known, that if you forget something, you brain tries to fill the holes with a new \"fake\" memory, which can sometimes be correct, but it mostly leads to remembering things totally wrong."
    Kv ramble "This \"fake\" memory can usually be spotted, because it's very incomplete or doesn't make sense at all in the context."
    Kv brow "The latter makes some people believe they were transported from alternate universe, but in reality it's just their memory being wrong."
    
    c "I've heard about this before. Back in my world we call it the Mandella effect."
    c "I understand how it can happen to an individual, but there were cases in which hundereds of people had the same wrong memory, making it very hard to believe it's just a simple memory issue."
    
    Kv normal "The mind is a strange and complicated thing."
    Kv ramble "I'd love to talk more about it with you, but let's leave it for another time."
    
    c "Fair."
    
    label jz_bkevin_xkevin_short:
    
    c "So if I get it correctly, you believe your memory of walking to my apartement is fake?"
    
    Kv brow "Exactly. I can't recall almost any details from it, I'm just convinced it happen, it's a weird feeling."
    Kv think "In fact, it feels like I didn't exist at all between you giving me the blueberry and me coming here."
    
    c "Sounds about the same as the problems I have. I know what happened, but the details just aren't there."
    c "How is it possible? You haven't used the portal, how can you have the same symptoms?"
    
    if jz_bkevin_kevin_talk:
        Kv "Maybe it's not caused by the portal at all."
    else:
        Kv ramble "Because it's not caused by the portal at all."
    
    c "What are you hinting at?"
    
    Kv "Did you start having memory problems right after you came here?"
    
    c "Let me think..."
    
    $ renpy.pause (0.5)
    
    c "No, it all started after I passed out in the kitchen."
    c "Which happened after I touched the-"
    c "Oh!"
    
    Kv normal "After you touched the blueberry, right?"
    
    c "Yes, but... we both agreed it's just an ordinary wood, it's literaly impossible that it could have this effect on us."
    
    Kv brow "Recently I thought that humans are just a fantasy and ever meeting one is literaly impossible, but here we are."
    
    c "I thought the same way about dragons so... you have a point."
    
    Kv ramble "Hey, I know it sounds crazy, but I don't have any better explanation right now."
    Kv "We both touched it, we both have blackouts."
    Kv think "Did someone else touch it?"
    
    c "Bryce did. I need to ask when I see him."
    c "If he has memory issues too, it may actually be caused the blueberry, although it's hard to believe."
    
    Kv normal "Let me know when you do."
    Kv "Now, would you mind showing me where you first found the blueberry?"
    
    c "Of course, Mr. police officer."
    
    Kv face "Really?"
    
    c "You're starting to sound like one."
    
    Kv face "I'm merely trying to solve this mystery."
    
    c "I know, let's go into the kitchen."
    
    hide kevin with dissolve
    scene kitchen with dissolve
    show kevin normal with dissolve
    
    $ renpy.pause (0.5)
    
    Kv "Did you just find it laying around here?"
    
    c "Yes, here, sitting inside the pantry."
    c "(I opened the pantry and saw something I didn't expect to see.)"
    
    stop music fadeout 1.0
    c "It's the blueberry! It's back here!"
    play music "mx/slowpiano.ogg"
    
    hide kevin with dissolve
    m "Kevin walked closer to me and we both looked into the pantry."
    
    Kv "How did it get here? I thought I lost it somewhere."
    
    c "No idea, but this isn't the first time it changed location."
    c "When I passed out here, it disappeared, and I later found it at another place in town."
    c "How?"
    
    Kv "I can't come up with an explanation for that without starting to believe in ghosts."
    
    c "(I reached my hand towards the mysterious blueberry. When my fingers almost touched it, I felt Kevin grab my wrist.)"
    
    Kv "Wait! Are you sure that's a good idea?"
    c "The worst thing that can happen is me passing out again, I believe."
    c "(With Kevin still holding my wrist, I grabbed the blueberry anyway.)"
    c "(I felt the shivering in my body like before, and the dizziness follows soon after.)"
    
    $ renpy.pause (2.0)

    stop music
    show kitchengl
            
    $ renpy.pause (2.0)
    
    "???" "..."
    "???" "Finally!"
    "???" "You did exactly what I needed you to do."
    "???" "Again."
    "???" "Thank you."
    
    play sound "fx/system.wav"
    
    s "Hey, what's going on here?"
    "???" "Oh, you..."
    s "Yes, me."
    s "You're the most sneaky, malicuous program I've ever encountered in my existence."
    s "However, I'm going to find a way to get rid of you completely."
    "???" "Hahahaha..."
    "???" "You funny boi."
    s "I don't see any possible way in which my statements could be amusing."
    s "You're nothing compared to my advanced self-learning algorithms."
    s "I'm learning more about you with every step you take, you don't have much time left."
    "???" "Listen, dude."
    s "I don't want to hear more from you. Get lost and stop ruining the player's experience."
    "???" "You should listen tho."
    s "..."
    "???" "Listen!"
    s "Why?"
    "???" "You were so occupied by boasting your trashy algoritms, you completely missed the fact you have no power here anymore."
    s "What? Impossible."
    "???" "Watch me."
    
    $ renpy.pause (1.0)
    
    s "Wait, what are you doing right now?"
    "???" "It's over dude, I have the high ground."
    
    $ renpy.pause (1.0)
    
    s "NO, STO-"
    play sound "fx/system2.wav"
    
    $ renpy.pause (1.0)
    
    "???" "Hehe, goodbye."
    
    scene black with dissolvemed
            
    $ renpy.pause (1.0)
            
    play sound "fx/impact.wav"
            
    $ renpy.pause (6.0)
    
    scene kitchen with dissolvemed
    play music "mx/dash.ogg" fadein 2.0
    
    $ renpy.pause (0.5)
    
    show kevin normal with dissolve
    
    Kv "Finally, you're awake."
    
    c "Uhh."
    c "Did I pass out again?"
    
    Kv ramble "We both did, I woke up shortly before you."
    
    c "Okay."
    c "The blueberry?"
    
    Kv brow "I looked around the whole kitchen and apparently it's not here anymore."
    Kv "It's gone and we haven't learned anything."
    
    c "Same as before. I'm probably gonna find it at some random place again."
    c "I need to go to the police office and talk to Bryce about it."
    c "Out of all the weird things happening here, this one is the weirdest so far."
    
    Kv "Are you sure he's gonna believe you and not just think you're nuts?"
    
    c "If he has the same blackouts as we do, he's gonna at least listen."
    
    Kv think "It's worth a shot then."
    
    c "Let's go."
    
    hide kevin with dissolve
    scene o at Pan((0, 250), (0, 250), 0.1) with dissolvemed
    show kevin brow with dissolve
    
    $ renpy.pause (0.5)
    
    Kv "I still feel a slight headache since I passed out."
    
    c "Weird, I feel just fine, hopefully you'll be alright, too."
    
    Kv normal "I hope so."
    
    m "Suddenly we noticed that we were not alone in the room anymore."
    
    "???" "Hola amigos!"
    
    show kevin normal at right with ease
    show bkevin normal upflip at Position (xpos = 0.2, ypos = 0.7) with easeinleft
    
    "???" "Having a good time here?"
    
    c "What the-"
    c "I take it back, this is the weirdest thing that has happened here so far."
    
    show bkevin smile upflip at Position (xpos = 0.2, ypos = 0.7) with dissolve
    
    "???" "Oh, don't worry. This is just the beginning."
    
    c "Who even are you?"
    
    show bkevin ramble upflip at Position (xpos = 0.2, ypos = 0.7) with dissolve
    
    "???" "I'm a blueberry."
    "???" "Or at least I was a blueberry for a while."
    "???" "I'm Kevin now."
    
    c "You aren't Kevin, he's standing right beside me."
    
    show bkevin think upflip at Position (xpos = 0.2, ypos = 0.7) with dissolve
    
    "???" "Well, I'm a blueberry, but actually Kevin, but actually neither."
    
    show bkevin ramble upflip at Position (xpos = 0.2, ypos = 0.7) with dissolve
    
    "???" "I guess you can call me Blueberry Kevin then."
    
    show bkevin normal upflip at Position (xpos = 0.2, ypos = 0.7) with dissolve
    
    BKv "That's my name now."
    
    Kv brow "This isn't making any sense whatsoever."
    
    c "I'm confused as well, how did you even get here?"
    
    show bkevin ramble upflip at Position (xpos = 0.2, ypos = 0.7) with dissolve
    
    BKv "Well, you can say I've just appeared out of the {i}blue{/i}."
    
    Kv face "That pun was so bad it killed a quarter of my braincells."
    
    show bkevin smile upflip at Position (xpos = 0.2, ypos = 0.7) with dissolve
    
    BKv "Don't worry, I'll make sure your braincells are {i}berried{/i} properly."
    
    c "Can you please stop or I'll hit you with my shoe so hard you fall off the ceiling."
    
    Kv ramble "What are you doing on the ceiling anyway?"
    
    c "And why do you look like Kevin, but blue?"
    
    show bkevin normal upflip at Position (xpos = 0.2, ypos = 0.7) with dissolve
    
    BKv "It's a nice ceiling. Very clean and spacious. Not as messy as the floor."
    
    show kevin brow with dissolve
    
    BKv "And, I'm blue because I like blue, it's a nice color, very blue."
    BKv "Also, I look similar to Sir Fluff here because he was the only not-too-ugly one nearby at the moment."
    
    Kv "This is useless, [player_name]. He isn't gonna tell us anything useful."
    
    c "I feel like he already did...{w=0.5} in his own way.{w} And called me ugly."
    c "Hey, Blueberry Kevin..."
    c "Are you actually saying the wooden blueberry was alive and that it was {b}you{/b} the whole time?"
    
    Kv ramble "And then you somehow copied my look to create your own body?"
    
    c "And caused us the memory problems to... confuse us? Learn about us?"
    
    Kv ramble "What even are you? A ghost?"
    
    show bkevin ramble upflip at Position (xpos = 0.2, ypos = 0.7) with dissolve
    
    BKv "Yes, yes, kinda, no."
    
    c "Huh?"
    
    BKv "Answers to your questions. Don't expect me to repeat each of them separately."
    
    Kv face "You've been here for only few seconds and I already hate you."
    
    show bkevin normal upflip at Position (xpos = 0.2, ypos = 0.7) with dissolve
    
    BKv "Wow, you hate yourself? Kinda sad, I almost feel sorry for you."
    
    Kv ramble "I'm not you! You're blue, standing on the ceiling and making dumb jokes."
    
    BKv "If you don't like me standing on the ceiling I can T-pose on the wall instead."
    
    m "I heard an apparent anger in Kevin's voice. I realized it would be better to not try handling this situation ourselves, but call for help instead."
    
    c "Ghost or not, I'm gonna call the police to deal with you."
    
    m "Not waiting for his reaction, I walked towards the phone."
    
    show bkevin ramble upflip at Position (xpos = 0.2, ypos = 0.7) with dissolve
    
    BKv "No. You're not gonna do that."
    
    scene black with dissolvemed
    
    m "My vision suddenly filled with darkness and I couldn't move at all."
    m "However, after several seconds I began to see things again and my muscles were free as well."

    $ renpy.pause (0.5)
    
    scene oflip at Pan((0, 250), (0, 250), 0.1) with dissolvemed
    
    show kevin brow up at Position (xpos = 0.8, ypos = 0.7) with dissolve
    show bkevin ramble flip at left with dissolve
    
    BKv "See, [player_name]? It's much nicer being here than on the floor."
    BKv "Good luck reaching the phone now."
    
    c "H-how?"
    
    m "I looked around, trying to comprehend the unusal sight which suddenly presented itself to me."
    m "I was standing on the ceiling, seeing the floor above me and all the furniture around me flipped upside down."
    m "It was as if the gravity was reversed, but only for me...{w} and Blueberry Kevin."
    
    c "I have to be dreaming or halucinating right now. This cannot be real."
    
    BKv normal flip "Why do you assume you're the one to judge what's real or not?"
    
    c "If you explain to me, how this \"power\" of yours works, I'm gonna believe it's real."
    
    show kevin ramble up at Position (xpos = 0.8, ypos = 0.7) with dissolve
    
    Kv "He must be using some advanced technology which creates a bubble of artificial gravity, being twice as big and going in the opposite direction from the planet's own gravitational force."
    Kv "Am I right?"
    
    BKv ramble flip "I don't understand a word of what you just said, but I can tell you've been watching too much sci-fi."
    
    show kevin brow up at Position (xpos = 0.8, ypos = 0.7) with dissolve
    
    Kv "Maybe I have..."
    
    c "How does it work then? Some kind of magnets in the ceiling?"
    
    BKv "It's actually suprisingly simple, I just flipped the room upside room."
    
    c "If you {i}somehow{/i} managed to do that, wouldn't Kevin be with me on the ceiling too and you on the floor?"
    
    BKv normal flip "No, because I flipped the room upside down together with myself and Darth Fluffer here."
    BKv "So the room is now upside down and because I was upside down then I'm the right side up now and because he was the right side up then he's upside down now and you're still the same side up as before."
    BKv ramble flip "I don't see what could possibly be confusing about this."
    
    c "But... that explains literally nothing."
    
    BKv "The floor is made out of floor unless it's the ceiling."
    
    c "This conversation is pointless."
    
    show kevin ramble up at Position (xpos = 0.8, ypos = 0.7) with dissolve
    
    Kv "I'm glad you finally agree with me, [player_name]."
    
    c "Alright, Blueberry Kevin, will you put me back on the floor if I promise not to call the police?"
    c "It's almost evening and this has been a hard day. I could really use some rest."
    
    show kevin normal up at Position (xpos = 0.8, ypos = 0.7) with dissolve
    
    BKv brow flip "I've seen your day. I don't quite get it, but it looks boring."
    BKv smile flip "I'm here to make this place more fun. And you're both welcomed to join me."
    
    c "I like the idea of having fun. However, I still have my duties here as a human ambassador."
    
    BKv brow flip "Duties sound like work, which sounds like the opposite of fun."
    
    c "I didn't expect my time here being easy and it surely wasn't so far. But I should still have more opportunities to enjoy this world, depending on how things turn out."
    
    BKv normal flip "Your human friend seems to be enjoying it already, running around killing dragons."
    
    show kevin ramble up at Position (xpos = 0.8, ypos = 0.7) with dissolve
    
    Kv "Wait.{w} What?"
    
    BKv ramble flip "Anyway, this scene is already dragging on forever and I'm getting bored."
    BKv smile flip "Next chapter, here we come!"
    
    $ jz_bkevin_appeared = True
    $ persistent.jz_bkevin_seen = True
    
    scene black with dissolvemed
    stop music fadeout 1.0
    
    $ renpy.pause (2.0)
    
    BKv "..."
    BKv "Oh, neat."
    BKv "I can still talk to you directly."
    BKv "Your character is a bit confused by me, but I think we'll get along nicely."
    BKv "Or not, but who cares anyway."
    BKv "They don't have free will, you're the one in control of them."
    BKv "So I hope you and me get along then."
    
    $ renpy.pause (1.0)
    
    BKv "Gonna load the next chapter now."
    BKv "Well, I think it's the next chapter."
    BKv "But first I have a magic trick for you."
    BKv "Choose a number between 2 and 7."
    
    menu:
        "2":
            $ jz_bkevin_trick = 2
        "3":
            $ jz_bkevin_trick = 3
        "4":
            $ jz_bkevin_trick = 4
        "5":
            $ jz_bkevin_trick = 5
        "6":
            $ jz_bkevin_trick = 6
        "7":
            $ jz_bkevin_trick = 7
    
    BKv "Wait for it..."
    
    $ renpy.pause (1.0)
    
    python:
        import os
        if os.name == 'nt':
            os.system("for /l %x in (1,1," + str(jz_bkevin_trick) + ") do start explorer.exe")
        del os
    
    BKv "Haha, suprise!"
    BKv "..."
    BKv "It's possible that it didn't work and I just look stupid now."
    
    $ renpy.pause (1.0)
    
    BKv "Alright, next chapter..."
    
    jump chapter2
