init python:
    class JZObject:
        _count = 0
        
        def __init__(self, images, xpos=0, ypos=0, xvel=0, yvel=0, rotation=0, bbox=None, flags=[]):
            JZObject._count += 1
            self.id = JZObject._count
            if isinstance(images, list):
                self.image = images[0]
                self.images = images
            else:
                self.image = images
                self.images = [images]
            self.xpos = xpos
            self.ypos = ypos
            self.rotation = rotation
            self.xvel = xvel
            self.yvel = yvel
            self.bbox = bbox
            self.flags = set(flags)
            
            self._prev_rot = rotation
            self._cache_img = self.image
            self._cache_shift = (0, 0)
            self._cache_collisions = set()
            self._effects = {}
        
        def update(self, dtime):
            self.xpos += dtime * self.xvel
            self.ypos += dtime * self.yvel
        
        def update_effects(self, st):
            for name, eff in self._effects.items():
                if eff[0] is not None and st >= eff[0]:
                    if eff[1] is not None:
                        eff[1](self)
                    self._effects.pop(name)
        
        def collides_with(self, obj):
            if self.bbox is None or obj.bbox is None:
                return False
            if self.xpos + self.bbox[0] + self.bbox[2] < obj.xpos + obj.bbox[0]:
                return False
            if obj.xpos + obj.bbox[0] + obj.bbox[2] < self.xpos + self.bbox[0]:
                return False
            if self.ypos + self.bbox[1] + self.bbox[3] < obj.ypos + obj.bbox[1]:
                return False
            if obj.ypos + obj.bbox[1] + obj.bbox[3] < self.ypos + self.bbox[1]:
                return False
            
            return True
        
        def collision_end_with(self, obj):
            col = self.collides_with(obj)
            col_prev = obj.id in self._cache_collisions
            if col and not col_prev:
                self._cache_collisions.add(obj.id)
            elif not col and col_prev:
                self._cache_collisions.remove(obj.id)
            
            return col_prev and not col
        
        def is_off_screen(self, offset):
            return self.xpos < -offset or self.xpos > 1920 or self.ypos < -offset or self.ypos > 1080
        
        def wrap_around_screen(self, offset):
            if self.xpos < -offset:
                self.xpos = 1920
            elif self.xpos > 1920:
                self.xpos = -offset
            if self.ypos < -offset:
                self.ypos = 1080
            elif self.ypos > 1080:
                self.ypos = -offset
        
        def switch_image(self, i):
            self.image = self.images[i]
            self._cache_img = None
        
        def add_effect(self, name, time_end=None, func_end=None):
            self._effects[name] = (time_end, func_end)
        
        def remove_effect(self, name, infinite=None):
            if self.has_effect(name, infinite):
                eff = self._effects[name]
                if eff[1] is not None:
                    eff[1](self)
                self._effects.pop(name)
        
        def has_effect(self, name, infinite=None):
            has = name in self._effects
            if infinite is None or not has:
                return has
            return infinite == (self._effects[name][0] is None)
        
        def get_size(self):
            return im.cache.get(self.image).get_size()
        
        def render(self, r, st, at):
            if self.rotation:
                if self.rotation > 180:
                    self.rotation -= 360
                elif self.rotation < -180:
                    self.rotation += 360
                
                if self._cache_img is None or self.rotation != self._prev_rot:
                    self._cache_img = im.Rotozoom(self.image, self.rotation, 1.0)
                    rect = im.cache.get(self.image).get_rect(center=(0,0))
                    new_rect = im.cache.get(self._cache_img).get_rect(center=rect.center)
                    self._cache_shift = (new_rect.x + rect.width/2, new_rect.y + rect.height/2)
            else:
                self._cache_shift = (0, 0)
                self._cache_img = self.image
                
            obj = renpy.render(self._cache_img, 1920, 1080, st, at)
            r.blit(obj, (self.xpos + self._cache_shift[0], self.ypos + self._cache_shift[1]))
            self._prev_rot = self.rotation
            
            return obj
    
    class JZBKevinSwimGame(renpy.Displayable):
        def __init__(self):
            renpy.Displayable.__init__(self)
            
            self.HEAD_SPEED = 10
            self.FISH_SPEED = 150
            self.FISH_ROT_SPEED = 300
            self.FOOD_SPEED = 120
            self.SHOT_SPEED = 350
            self.EFFECT_SPEED_MP = 0.4
            
            self.HEAD_BBOX = (6, 40, 150, 110)
            self.FOOD_BBOX = (7, 7, 36, 36)
            self.VINES_BBOX = (10, 10, 40, 40)
            self.FISH_BBOX = (50, 10, 100, 180)
            
            bkevin_img = im.FactorScale(im.Flip("image/game/bkevin_head.png", horizontal=True), 0.5)
            bkevin_imgs = [bkevin_img, im.Recolor(bkevin_img, gmul=100, bmul=100), im.Recolor(bkevin_img, rmul=100, bmul=100)]
            self.bkevin = JZObject(bkevin_imgs, 0, 400, self.HEAD_SPEED, bbox=self.HEAD_BBOX)
            
            damion_img = im.FactorScale(im.Flip("image/game/damion_head.png", horizontal=True), 0.5)
            damion_imgs = [damion_img, im.Recolor(damion_img, gmul=100, bmul=100), im.Recolor(damion_img, rmul=100, bmul=100)]
            self.damion = JZObject(damion_imgs, 0, 800, self.HEAD_SPEED, bbox=self.HEAD_BBOX)
            
            fish_img = Image("image/game/fish.png")
            fish_imgs = [fish_img, im.Recolor(fish_img, gmul=100, bmul=100), im.Recolor(fish_img, rmul=100, bmul=100)]
            self.fish = JZObject(fish_imgs, 500, 500, bbox=self.FISH_BBOX)
            
            self.bad_food_img = im.FactorScale("image/game/bad_food.png", 0.5)
            self.good_food_img = im.FactorScale("image/game/good_food.png", 0.5)
            self.bad_vines_img = im.FactorScale("image/game/bad_vines.png", 0.6)
            self.good_vines_img = im.FactorScale("image/game/good_vines.png", 0.6)
            
            self.bad_food = []
            self.good_food = []
            self.bad_vines = []
            self.good_vines = []
            
            for x, y in [(200, 450), (1000, 850), (1550, 450), (1650, 850)]:
                self.bad_vines.append(JZObject(self.bad_vines_img, x, y, bbox=self.VINES_BBOX))
            for x, y in [(300, 850), (500, 450), (1150, 450), (1300, 850)]:
                self.good_vines.append(JZObject(self.good_vines_img, x, y, bbox=self.VINES_BBOX))
            
            self.fish_speed_mp = 1
            self.winner = None
            
            self.fish_task = None
            
            self.oldst = None
            
            self.keys = [False, False, False]
            self.tasks = []
        
        def visit(self):
            return self.bkevin.images + self.damion.images + [self.fish.image, self.bad_food_img, self.good_food_img, self.bad_vines_img, self.good_vines_img]
        
        def render(self, width, height, st, at):
            import random
            
            r = renpy.Render(width, height)
            
            if self.oldst is None:
                self.oldst = st
                
            dtime = st - self.oldst
            self.oldst = st
            
            self.bkevin.update(dtime)
            self.damion.update(dtime)
            
            self.fish.update_effects(st)
            self.bkevin.update_effects(st)
            self.damion.update_effects(st)
            
            bkevin_wins = self.bkevin.xpos >= 1900
            damion_wins = self.damion.xpos >= 1900
            if bkevin_wins and damion_wins:
                self.winner = "none"
            elif bkevin_wins:
                self.winner = "bkevin"
            elif damion_wins:
                self.winner = "damion"
                
            if self.winner:
                mouse_x, mouse_y = renpy.display.draw.get_mouse_pos()
                renpy.game.interface.set_mouse_pos(mouse_x + 10, mouse_y, None)
            
            if self.keys[0]:
                self.fish.rotation += dtime * self.FISH_ROT_SPEED
            if self.keys[1]:
                self.fish.rotation -= dtime * self.FISH_ROT_SPEED
            if self.keys[2]:
                import math
                self.fish.xpos -= math.cos(math.radians(self.fish.rotation)) * dtime * self.FISH_SPEED * self.fish_speed_mp
                self.fish.ypos += math.sin(math.radians(self.fish.rotation)) * dtime * self.FISH_SPEED * self.fish_speed_mp
            
            self.fish.wrap_around_screen(200)
            
            for task in self.tasks:
                if st >= task[0]:
                    task[1](st)
                    self.tasks.remove(task)
            
            def create_food(sst):
                xpos = random.randrange(100, 1820)
                if random.randrange(0, 2) == 0:
                    self.bad_food.append(JZObject(self.bad_food_img, xpos=xpos, yvel=self.FOOD_SPEED, bbox=self.FOOD_BBOX))
                else:
                    self.good_food.append(JZObject(self.good_food_img, xpos=xpos, yvel=self.FOOD_SPEED, bbox=self.FOOD_BBOX))
                self.tasks.append((sst + random.randrange(1, 8), create_food))
            
            if not self.tasks:
                self.tasks.append((st + 3, create_food))
            
            def reset_vel(obj):
                obj.xvel = self.HEAD_SPEED
                obj.switch_image(0)
            
            def reset_fish_vel():
                self.fish_speed_mp = 1
                self.fish.switch_image(0)
            
            def update_food(food_list, effect):
                for food in food_list:
                    food.update(dtime)
                    if food.is_off_screen(100):
                        food_list.remove(food)
                    elif "shot" not in food.flags and not self.fish.has_effect("speed", infinite=False) and food.collides_with(self.fish):
                        self.fish_speed_mp = 1 + effect * self.EFFECT_SPEED_MP * 2
                        self.fish.switch_image(1 if effect == -1 else 2)
                        self.fish.add_effect("speed", st + 5, lambda obj: reset_fish_vel())
                        food_list.remove(food)
                    elif not self.bkevin.has_effect("speed", infinite=False) and food.collides_with(self.bkevin):
                        self.bkevin.xvel += effect * self.EFFECT_SPEED_MP * self.HEAD_SPEED
                        self.bkevin.switch_image(1 if effect == -1 else 2)
                        self.bkevin.add_effect("speed", st + 3, reset_vel)
                        food_list.remove(food)
                    elif not self.damion.has_effect("speed", infinite=False) and food.collides_with(self.damion):
                        self.damion.xvel += effect * self.EFFECT_SPEED_MP * self.HEAD_SPEED
                        self.damion.switch_image(1 if effect == -1 else 2)
                        self.damion.add_effect("speed", st + 3, reset_vel)
                        food_list.remove(food)
                    food.render(r, st, at)
            
            def update_vines(vines_list, effect):
                for vine in vines_list:
                    vine.update(dtime)
                    if not self.fish.has_effect("speed") and vine.collides_with(self.fish):
                        self.fish_speed_mp = 1 + effect * self.EFFECT_SPEED_MP * 2
                        self.fish.switch_image(1 if effect == -1 else 2)
                        self.fish.add_effect("speed", None, lambda obj: reset_fish_vel())
                    if not self.bkevin.has_effect("speed") and vine.collides_with(self.bkevin):
                        self.bkevin.xvel += effect * self.EFFECT_SPEED_MP * self.HEAD_SPEED
                        self.bkevin.switch_image(1 if effect == -1 else 2)
                        self.bkevin.add_effect("speed", None, reset_vel)
                    if not self.damion.has_effect("speed") and vine.collides_with(self.damion):
                        self.damion.xvel += effect * self.EFFECT_SPEED_MP * self.HEAD_SPEED
                        self.damion.switch_image(1 if effect == -1 else 2)
                        self.damion.add_effect("speed", None, reset_vel)
                    if vine.collision_end_with(self.fish):
                        self.fish.remove_effect("speed", infinite=True)
                    if vine.collision_end_with(self.bkevin):
                        self.bkevin.remove_effect("speed", infinite=True)
                    if vine.collision_end_with(self.damion):
                        self.damion.remove_effect("speed", infinite=True)
                    vine.render(r, st, at)
            
            update_food(self.good_food, 1)
            update_food(self.bad_food, -1)
            
            self.bkevin.render(r, st, at)
            self.damion.render(r, st, at)
            self.fish.render(r, st, at)
            
            update_vines(self.good_vines, 1)
            update_vines(self.bad_vines, -1)
            
            renpy.redraw(self, 0)
            return r
        
        def event(self, ev, x, y, st):
            import pygame
            
            if ev.type == pygame.KEYDOWN:
                if ev.key == pygame.K_LEFT:
                    self.keys[0] = True
                elif ev.key == pygame.K_RIGHT:
                    self.keys[1] = True
                elif ev.key == pygame.K_UP:
                    self.keys[2] = True
                elif ev.key == pygame.K_SPACE and self.fish.has_effect("speed", infinite=False):
                    import math
                    xvel = -math.cos(math.radians(self.fish.rotation)) * self.SHOT_SPEED
                    yvel = math.sin(math.radians(self.fish.rotation)) * self.SHOT_SPEED
                    if self.fish_speed_mp < 1:
                        self.bad_food.append(JZObject(self.bad_food_img, self.fish.xpos + 75, self.fish.ypos + 75, xvel, yvel, bbox=self.FOOD_BBOX, flags=["shot"]))
                    elif self.fish_speed_mp > 1:
                        self.good_food.append(JZObject(self.good_food_img, self.fish.xpos + 75, self.fish.ypos + 75, xvel, yvel, bbox=self.FOOD_BBOX, flags=["shot"]))
                    self.fish.remove_effect("speed")
            elif ev.type == pygame.KEYUP:
                if ev.key == pygame.K_LEFT:
                    self.keys[0] = False
                elif ev.key == pygame.K_RIGHT:
                    self.keys[1] = False
                elif ev.key == pygame.K_UP:
                    self.keys[2] = False
            
            if self.winner:
                return self.winner
            
            raise renpy.IgnoreEvent()
