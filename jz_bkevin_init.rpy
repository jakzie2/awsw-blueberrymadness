image town6brgl = "bg/town6brgl.png"
image oflip = "bg/of.png"
image kitchengl = "bg/kitchengl.png"
image underwatersea = "bg/underwatersea.png"

image kevin normal up = im.Flip("cr/kevin_normal.png", vertical=True)
image kevin face up = im.Flip("cr/kevin_face.png", vertical=True)
image kevin brow up = im.Flip("cr/kevin_brow.png", vertical=True)
image kevin ramble up = im.Flip("cr/kevin_ramble.png", vertical=True)

image damion normal error = "cr/damion_normal_error.png"
image damion face error = "cr/damion_face_error.png"
image damion arrogant error = "cr/damion_arrogant_error.png"

image damion normal error flip = im.Flip("cr/damion_normal_error.png", horizontal=True)
image damion arrogant error flip = im.Flip("cr/damion_arrogant_error.png", horizontal=True)

image damion normal up = im.Flip("cr/damion_normal.png", vertical=True)
image damion normal upflip = im.Flip("cr/damion_normal.png", vertical=True, horizontal=True)

image damion normal error upflip = im.Flip("cr/damion_normal_error.png", vertical=True, horizontal=True)

image bkevin normal = "cr/bkevin_normal.png"
image bkevin face = "cr/bkevin_face.png"
image bkevin brow = "cr/bkevin_brow.png"
image bkevin ramble = "cr/bkevin_ramble.png"
image bkevin think = "cr/bkevin_think.png"
image bkevin smile = "cr/bkevin_smile.png"
image bkevin sad = "cr/bkevin_sad.png"
image bkevin none = "cr/bkevin_none.png"

image bkevin normal flip = im.Flip("cr/bkevin_normal.png", horizontal=True)
image bkevin face flip = im.Flip("cr/bkevin_face.png", horizontal=True)
image bkevin brow flip = im.Flip("cr/bkevin_brow.png", horizontal=True)
image bkevin ramble flip = im.Flip("cr/bkevin_ramble.png", horizontal=True)
image bkevin think flip = im.Flip("cr/bkevin_think.png", horizontal=True)
image bkevin smile flip = im.Flip("cr/bkevin_smile.png", horizontal=True)
image bkevin sad flip = im.Flip("cr/bkevin_sad.png", horizontal=True)
image bkevin none flip = im.Flip("cr/bkevin_none.png", horizontal=True)

image bkevin normal up = im.Flip("cr/bkevin_normal.png", vertical=True)

image bkevin normal upflip = im.Flip("cr/bkevin_normal.png", vertical=True, horizontal=True)
image bkevin ramble upflip = im.Flip("cr/bkevin_ramble.png", vertical=True, horizontal=True)
image bkevin think upflip = im.Flip("cr/bkevin_think.png", vertical=True, horizontal=True)
image bkevin smile upflip = im.Flip("cr/bkevin_smile.png", vertical=True, horizontal=True)

image bkevin normal coat = "cr/bkevin_normal_coat.png"
image bkevin smile coat = "cr/bkevin_smile_coat.png"
image bkevin ramble coat = "cr/bkevin_ramble_coat.png"

image bkevin normal coat flip = im.Flip("cr/bkevin_normal_coat.png", horizontal=True)
image bkevin ramble coat flip = im.Flip("cr/bkevin_ramble_coat.png", horizontal=True)
image bkevin smile coat flip = im.Flip("cr/bkevin_smile_coat.png", horizontal=True)

image bkevin normal coat upflip = im.Flip("cr/bkevin_normal_coat.png", vertical=True, horizontal=True)

define BKv = Character(_("Blueberry Kevin"), color="#333369", image="bkevin")

init python:
    jz_unblueberry = True
    jz_bkevin_kevinmenu_stage = 0
    jz_bkevin_kevinmenu_first = True
    jz_bkevin_appeared = False
    jz_bkevin_bkev_points = 0
    jz_bkevin_system_points = 0
    jz_bkevin_not_throw = False
    jz_bkevin_throw_count = 0
    jz_bkevin_kevin_talk = False
    jz_bkevin_bryce_status = "none"
    jz_bkevin_chase_join = False
    jz_bkevin_swimgame_song = False
    jz_bkevin_swimgame_result = "none"

label jz_bkevin_menu:
    if persistent.jz_bkevin_seen:
        play music "mx/blue_sayingod24.mp3"
    else:
        play music "mx/menu.ogg"
    
    return

label jz_bkevin_test:
    $ jz_init_errors = 0 
    
    if persistent.remymet and not persistent.jz_bkevin_mod:
        $ jz_init_errors += 1
        $ jz_savepath = config.gamedir + "/saves"
        
        window show
        n "In order to play The Blueberry Madness mod, you need to remove the persistent save file containing data about your progress. Easiest way to do it is to create a new profile using LMAO (Lightweight Mod and Addon Organizer).\nYou can follow the step-by-step tutorial at http://jakzie2.8u.cz/awsw_mods/bkevin/"
    
    python:
        jz_mods_whitelist = ["Core", "The Blueberry Madness", "LMAO Tools", "MagmaLink", "CRAP"]
        jz_mods_incompatible = []
        for mod in modinfo.get_mods().keys():
            if mod not in jz_mods_whitelist:
                jz_mods_incompatible.append(mod)

    if len(jz_mods_incompatible) > 0:
        $ jz_init_errors += 1
        $ jz_mods_incompatible_string = ", ".join(jz_mods_incompatible)
        
        nvl clear
        window show
        n "The Blueberry Madness detected incompatible mods. Please remove all following mods in order to play it:\n\n[jz_mods_incompatible_string]"
        
    if jz_init_errors > 0:
        return
    
    jump jz_bkevin_test_return

label jz_bkevin_skipintro:
    $ persistent.jz_bkevin_mod = True
    
    m "This mod doesn't make any changes to the intro, therefore you have the option to skip it entirely."
    
    menu:
        m "Do you wish to skip to the beginning of Chapter 1?"
        
        "Yes.":
            $ remystatus = "none"
            $ annastatus = "none"
            $ adinestatus = "none"
            $ brycestatus = "none"
            $ loremstatus = "none"

            $ remydead = False
            $ annadead = False
            $ adinedead = False
            $ brycedead = False
            $ loremdead = False
            
            $ persistent.remymet = True
            
            jump skipintro
        "No.":
            jump jz_bkevin_skipintro_return


label jz_bkevin_virusscan1:
    $ renpy.pause (0.5)
    
    stop music
    play sound "fx/system.wav"
    
    s "{i}System message{/i}: Warning, unknown threat detected."
    
    $ renpy.pause (0.5)
    
    s "{i}System message{/i}: Initiating virus scan{cps=*0.5}...{/cps}"
    
    $ renpy.pause (2.0)
    
    jump jz_bkevin_virusscan1_return

label jz_bkevin_virusscan2:
    scene black with fade
    $ renpy.pause (1.0)
    
    stop music
    play sound "fx/system.wav"
    
    s "{i}System message{/i}: Virus scan completed. 1 malicious program found."
    
    $ renpy.pause (0.5)
    
    s "{i}System message{/i}: Generating very polite but completely non-informative alert for the user{cps=*0.5}...{/cps}"
    
    $ renpy.pause (0.5)
    
    s "Sorry to interrupt your gameplay, but we're now experiencing some technical issues. Please don't turn off the game and wait until our questionably capable automatic algorithms resolve the problem. It can take anywhere from several seconds to several years."
    
    $ renpy.pause (0.5)
    
    s "{i}System message{/i}: Creating quarantine sector{cps=*0.5}...{/cps}"
    
    $ renpy.pause (3.0)
    
    s "{i}System message{/i}: New quarantine sector with ID \"123-BLUEBERRY\" created. Moving 1 threat into the quarantine sector{cps=*0.5}...{/cps}"
    
    $ renpy.pause (3.0)
    
    s "{i}System message{/i}: 1 threat moved into quarantine. No more threats detected. Generating another polite alert for probably frustrated and annoyed user."
    
    $ renpy.pause (0.5)
    
    s "Thank you for you patience, all issues were resolved. You can now finally continue your gameplay."
    s "{i}System message{/i}: Resuming gameplay{cps=*0.5}...{/cps}"
    
    $ renpy.pause (0.5)
    
    scene cafe with fade
    play music "mx/jazzy.ogg"
    
    jump jz_bkevin_virusscan2_end


label jz_bkevin_ending:
    nvl clear
    window show
    n "You've reached the end of The Blueberry Madness mod. This mod is in a very early version, so you're hopefully gonna see much more content in the future."
    n "I hope you enjoyed it and come back to play it when a new version is released."
    n "If you have any feedback, complaints or suggestions, let me know, so I can use them to make this mod better."
    nvl clear
    n "Special thanks to:"
    n "Eval - testing, grammar checking, sprites"
    n "Whisp, Venom, Engineer of Stupidity - ideas and sprites"
    n "Ryann - testing"
    n "sayingod24 - original menu music"
    nvl clear
    n "Thank you for playing and enjoy the rest of your day."
    n "- Jakzie"
    window hide
    nvl clear
    
    return
